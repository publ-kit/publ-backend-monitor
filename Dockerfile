FROM 581465796948.dkr.ecr.ap-northeast-2.amazonaws.com/publ-openresty-alpine:0.0.4 

RUN apk add --no-cache logrotate
RUN mkdir /data/html

copy ./conf /usr/local/openresty/nginx/conf
copy ./lua /usr/local/openresty/nginx/lua
copy ./html /data/html

EXPOSE 8888 8888
CMD ["/usr/local/openresty/nginx/sbin/nginx", "-g", "daemon off; master_process on;"]
