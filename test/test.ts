import * as aws from "aws-sdk";
import * as fs from "fs";

class test {

    bucket: string;
    s3: aws.S3;
    constructor(

    ) {
        //AWS_ACCESS_KEY=AKIAYOYQP6FKIGC6ZPOH;AWS_SECRET_KEY=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
        const env = process.env;
        this.bucket = 's3.publishingkit.net';
        aws.config.update({
            credentials: {
                accessKeyId: 'AKIAYOYQP6FKIGC6ZPOH',
                secretAccessKey: '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'
            },
            region: 'ap-northeast-2'
        });
        this.s3 = new aws.S3();
    }

    async checkLiveTs(
        streamKey: string,
        quality: number,
        start: number,
        end: number,
    ): Promise<aws.S3.Types.ObjectList> {
        const contents = await this.getRecordTargets(streamKey, quality, start-900);
        let lastModified = contents[contents.length-1].LastModified;
        console.log(lastModified);
        let loop = true;
        let checkModified = new Date();
        while (loop) {
            const targetTime = lastModified;
            const tmpContents = await this.getRecordTargets(streamKey, quality, new Date(targetTime).getTime() / 1000 - 5);
            contents.push(...tmpContents);

            lastModified = contents[contents.length - 1].LastModified;
            if (checkModified === lastModified) {
                loop = false;
                break;
            }
            checkModified = lastModified;
            if (new Date(lastModified) > new Date(end)) {
                loop = false;
                break;
            }
        }
        console.log(contents.length);
        const setContents = contents.filter((element, index)=> {
            return contents.indexOf(element)===index;
        });
        console.log(setContents.length);

        const checkLoop = Number(contents.length / 1000);
        for (let i = 1; i <= checkLoop + 1; i++) {
            // console.log(i * 1000);
            const start = (i-1) * 1000;
            let end = i * 1000;
            if (end > contents.length) {
                end = contents.length;
            }
            console.log(`${start} to ${end}`);
            console.log(contents.slice(start, end).length);
        }
        return contents;
    }

    async getRecordTargets(streamKey: string, quality: number, time: number) {
        const param: aws.S3.Types.ListObjectsV2Request = {
            Bucket: this.bucket,
            Prefix: `live/${streamKey}/${quality}`,
            StartAfter: `live/${streamKey}/${quality}/${time}.ts`,
            MaxKeys: 1000
        };
        const res = await this.s3.listObjectsV2(param).promise();
        // console.log(res);
        return res.Contents;
    }

    async getKeyPrefixExist() {
        const keyListStr = fs.readFileSync('test/prod.test.txt');
        const keyList = keyListStr.toString().split("\n");
        for (const key of keyList) {
            const param: aws.S3.Types.ListObjectsV2Request = {
                Bucket: this.bucket,
                Prefix: `live/${key}/1080`,
                MaxKeys: 1000
            };
            const res = await this.s3.listObjectsV2(param).promise();
            if (res.Contents.length > 0) {
                console.log(`${key}`);
            }
        }
    }
}

const start = new Date('2022-12-05 10:00:00').getTime();
const end = new Date('2022-12-05 14:26:33').getTime();
// new test().checkLiveTs('MzViYzk0ZjQtZTkzOS00OQ==', 1080, start, end).then(()=>{});
new test().getKeyPrefixExist().then(()=>{});
