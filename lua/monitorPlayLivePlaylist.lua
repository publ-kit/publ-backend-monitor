-- monitorGetEncoder.lua

local REDIS = require "resty.redis"
local redis = REDIS:new()
local cjson = require "cjson"
local json = cjson.new()
local ok, err = redis:connect(os.getenv('LIVE_REDIS'), os.getenv('REDIS_PORT'))
local streamKey = ngx.var.streamKey
local liveId = ngx.var.liveId

local playlistSaveKey = 'live:{'..streamKey..'}:playlist:{1080}'

function split(source, delimiters)
    local elements = {}
    local pattern = '([^'..delimiters..']+)'
    string.gsub(source, pattern, function(value)
        elements[#elements + 1] = value
    end)
    return elements
end

local playlistData, err = redis:get(playlistSaveKey)
local playlistStr = {}
local playlistDataStr = split(playlistData, "\n")
for line in pairs(playlistDataStr) do
    playlistStr[#playlistStr + 1] = playlistDataStr[line]
end

local playlist = ""
local envKeyUri = os.getenv('KEY_URI')
local keyUri = envKeyUri.."/live/key/"..liveId

for k, v in pairs(playlistStr) do
        if string.find(v, "ts") ~= nil then
        playlist = playlist .. v .."\n"
        else
                if string.find(v, "URI") ~= nil then
                        local keyData = split(v, ",")
                        local method = keyData[1]
                        local uri = "URI=".."\""..keyUri.."\""
                        local iv = keyData[3]
                        playlist = playlist .. method..","..uri..","..iv.."\n"
                else
                        playlist = playlist .. v .. "\n"
                end
        end
end

ngx.say(playlist)
