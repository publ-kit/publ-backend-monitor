-- get Lives
local psql = require "pgmoon"
local cjson = require "cjson"
local json = cjson.new()

function pgCon()
	local pgOption = {
		host = os.getenv('PG_HOST'),
		port = os.getenv('PG_PORT'),
		database = os.getenv('PG_DB'),
		user = os.getenv('PG_USER'),
		password = os.getenv('PG_PASS')
	}

	local pg = psql.new(pgOption)

	assert(pg:connect())

	return pg
end

function getLives(pg)
	local res = assert(pg:query('select id, title, cid, status, "startTime", "endTime", "streamKey", "metaData" from "Live" order by id'))
	if next(res) == nil then
		ngx.log(ngx.ERR, "empty")
		return json.empty_array
	else
		return res
	end
end


local pg = pgCon()
local res = getLives(pg)
ngx.header['Content-type'] = 'application/json'
local response = {
	data = res
}
ngx.say(json.encode(response))
