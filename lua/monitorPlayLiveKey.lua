-- monitorGetEncoder.lua

local psql = require "pgmoon"
local cjson = require "cjson"
local json = cjson.new()

function pgCon()
	local pgOption = {
		host = os.getenv('PG_HOST'),
		port = os.getenv('PG_PORT'),
		database = os.getenv('PG_DB'),
		user = os.getenv('PG_USER'),
		password = os.getenv('PG_PASS')
	}

	local pg = psql.new(pgOption)

	assert(pg:connect())

	return pg
end

local liveId = ngx.var.liveId

function getLives(pg, liveId)
	local res = assert(pg:query('select  "metaData" from "Live" where id ='..liveId))
	if next(res) == nil then
		ngx.log(ngx.ERR, "empty")
		return json.empty_array
	else
		return res
	end
end


local pg = pgCon()
local res = getLives(pg, liveId)

local key = res[1].metaData.key

ngx.say(key)
