-- monitorGetEncoder.lua

local REDIS = require "resty.redis"
local redis = REDIS:new()
local cjson = require "cjson"
local json = cjson.new()
local ok, err = redis:connect(os.getenv('LIVE_REDIS'), os.getenv('REDIS_PORT'))

local args, err = ngx.req.get_uri_args()

local liveId = args["id"]
local streamKey, err = redis:get('live:{'..liveId..'}:info')
local encoderData = redis:hgetall('live:log:{'..streamKey..'}')

local res = {}
local index = 0
local key = ""
for i in ipairs(encoderData) do
        if index == 0 then
                key = encoderData[i]
                index = 1
        else
                res[key] = encoderData[i]
                index = 0
        end
end
ngx.say(json.encode(res))
