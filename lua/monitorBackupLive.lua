-- monitorGetEncoder.lua

local REDIS = require "resty.redis"
local redis = REDIS:new()
local cjson = require "cjson"
local json = cjson.new()
local ok, err = redis:connect(os.getenv('LIVE_REDIS'), os.getenv('REDIS_PORT'))

local args, err = ngx.req.get_uri_args()
ngx.req.read_body()
local request_body = ngx.req.get_body_data()

ngx.log(ngx.ERR, request_body)
local data = json.decode(request_body)
local originLiveId = data['originLiveId']
local targetLiveId = data['targetLiveId']
local targetStreamKey = data['targetStreamKey']
local originStreamKey = data['originStreamKey']
local targetMetaData = data['targetMetaData']
local originCid = data['originCid']

redis:set('live:{'..originLiveId..'}:info', targetStreamKey)
redis:hset('live:drm:info', originCid, json.encode(targetMetaData))

ngx.say(json.encode(targetMetaData))
