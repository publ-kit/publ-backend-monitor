-- import
local common = require "Common"
local rabbitmq = require "amqp"
local cjson = require "cjson"
local json = cjson.new()

function mqCon(queue)
	local opts = {
		role = 'producer',
		exchange = '',
		routing_key = queue,
		queue = queue,
		user = os.getenv('MQ_USER'),
		password = os.getenv('MQ_PASS'),
		ssl = false
	}
	local mq = rabbitmq:new(opts)
	if not mq then
		return nil
	end

	local ok, err = mq:connect(os.getenv('MQ_HOST'),os.getenv('MQ_PORT'))
	if not ok then
		ngx.log(ngx.ERR, err)
		return nil
	end

	local ok, err = mq:setup()
	if not ok then
		ngx.log(ngx.ERR, err)
		return nil
	end

	return mq
end

function mqSend(mq, msg)
	local jsonMsg = json.encode(msg)
	ngx.log(ngx.ERR, jsonMsg)
	local ok, err = mq:publish(jsonMsg, {})
	if not ok then
		ngx.log(ngx.ERR, err)
		return nil
	end

	return ok
end

local mq = mqCon(os.getenv('MQ_NAME'))

function readBody()
	ngx.req.read_body()
	local payload = ngx.req.get_body_data()
	local body = json.decode(payload)
	local cid = body.cid
	if not cid then
		ngx.log(ngx.ERR, 'not found cid')
		return nil
	end
	local liveId = body.liveId
	if not liveId then
		ngx.log(ngx.ERR, 'not found live id')
		return nil
	end
	
	local broadcastStatus = body.bStatus
	if not broadcastStatus then
		ngx.log(ngx.ERR, 'not found broadcastStatus')
		return nil
	end
	
	local streamStatus = body.sStatus
	if not streamStatus then
		ngx.log(ngx.ERR, 'not found streamStatus')
		return nil
	end

	return {
		cid = cid,
		id = liveId,
		broadcastStatus = broadcastStatus,
		streamStatus = streamStatus
	}
end

local body = readBody()
if not body then
	ngx.log(ngx.ERR, 'invalid body')
end
local send = mqSend(mq, body)
ngx.log(ngx.ERR, send)
