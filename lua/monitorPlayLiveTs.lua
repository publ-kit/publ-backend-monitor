-- monitorGetEncoder.lua

local REDIS = require "resty.redis"
local redis = REDIS:new()
local cjson = require "cjson"
local json = cjson.new()
local base64 = require "base64"
local crypto = require "crypto"

local cfDomain = os.getenv('CF_DOMAIN')
local AES_KEY = os.getenv('AES_KEY')
local AES_IV = os.getenv('AES_IV')
local ok, err = redis:connect(os.getenv('LIVE_REDIS'), os.getenv('REDIS_PORT'))

local streamKey = ngx.var.streamKey
local liveId = ngx.var.liveId
local tmpTsName = ngx.var.tsName
local tsName = tmpTsName..'.ts'

local data = {
    tsName = tsName,
    liveId = liveId,
    quality = 1080,
    streamKey = streamKey,
    expire = os.time(os.date("!*t"))+10
}

local cipher = 'aes128'
local jsonStr = json.encode(data)
local ok, res = pcall(crypto.encrypt, cipher, jsonStr, AES_KEY, AES_IV)
local signedUrl = base64.encode(res)
local cfUrl = cfDomain.."/"..tsName

ngx.redirect(cfUrl.."?signedUrl="..signedUrl)
