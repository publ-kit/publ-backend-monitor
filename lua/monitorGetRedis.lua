-- get redis data

local REDIS = require "resty.redis"
local redis = REDIS:new()
local cjson = require "cjson"
local json = cjson.new()
local ok, err = redis:connect(os.getenv('SIG_REDIS'), os.getenv('REDIS_PORT'))

local sigDataList = redis:hgetall('signal.broadcasting')

local data = {}
if sigDataList == ngx.null then
	ngx.say(ngx.null)
else
	local key = ""
	local index = 1
	for i in ipairs(sigDataList) do
		if index == 1 then
			key = sigDataList[i]
			index = 2
		else
			data[key] = json.decode(sigDataList[i])
			index = 1
		end
	end
end
local response = {
	data = data
}
ngx.header["Content-type"] = "application/json"
ngx.say(json.encode(response))
