import {HttpException, Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {
    VODEntity,
    VODQualityEntity,
    VODQualityRepository,
    VODRepository,
    VODSubtitleEntity,
    VODSubtitleRepository
} from "./db";
import {Raw, Repository} from "typeorm";
import * as util from "util";
import * as aws from "aws-sdk";
import {RedisService} from "./db/redis";
import {Redis} from "ioredis";
import {JwtService} from "@nestjs/jwt";
import * as console from "console";

export class GetConsumerIdQuery {
    vid: string;
}
export class GetVODTokenQuery {
    consumerId: string;
}
export class VODQualityStatusRes {
    quality: number;
    status: boolean;
    playlist: boolean;
    s3: boolean;

    setResponse(quality?: number, status?: boolean, playlist?: boolean, s3?: boolean): this {
        if(quality) this.quality = quality;
        this.status = status;
        this.playlist = playlist;
        this.s3 = s3;
        return this;
    }
}

export class VODSubtitleStatusRes {
    language: string;
    status: number;
    isDefault: boolean;
    setResponse(subtitle: VODSubtitleEntity): this {
        const {status, language, isDefault} = subtitle;
        this.language = language;
        this.status = status;
        this.isDefault = isDefault;
        return this;
    }
}

export class VODStatusRes {
    vod: boolean;
    vodQuality: VODQualityStatusRes[];
    subtitle: VODSubtitleStatusRes[];
    multiPlaylist: boolean;
    setResponse(vod: boolean, vodQuality: VODQualityStatusRes[], multi: boolean, subtitle: VODSubtitleStatusRes[]): this {
        this.vod = vod;
        this.vodQuality = vodQuality;
        this.multiPlaylist = multi;
        this.subtitle = subtitle;
        return this;
    }
}

export class PagingModel {
    page?: number;
    size?: number;
    id?: number;
}

@Injectable()
export class VODService {

    private readonly s3: aws.S3;
    private readonly bucket: string;
    private readonly playlistKey: string;
    private readonly redis: Redis;
    private readonly playUrl: string;
    private readonly nodeEnv: string;
    private readonly subtitleUrl: string;
    private readonly subtitleInf: string;
    constructor(
        private readonly vodRepo: VODRepository,
        private readonly vodQualityRepo: VODQualityRepository,
        private readonly vodSubtitleRepo: VODSubtitleRepository,
        private readonly redisService: RedisService,
        private readonly jwtService: JwtService,
    ) {
        const env = process.env;
        this.bucket = env.BUCKET;
        aws.config.update({
            credentials: {
                accessKeyId: env.AWS_ACCESS,
                secretAccessKey: env.AWS_SECRET
            },
            region: env.AWS_REGION
        });
        this.s3 = new aws.S3();
        this.playlistKey = env.PLAYLIST_KEY;
        this.redis = redisService.client;
        this.playUrl = env.PLAY_URL;
        this.nodeEnv = env.ENV_NAME;
        this.subtitleUrl = env.SUBTITLE_URL;
        this.subtitleInf = env.SUBTITLE_INF;
    }

    async getVODs(query: PagingModel): Promise<any> {
        const {page, size, id} = query;
        const take =  size? size : 10;
        const skip = size * Number(page? page - 1 : 0);
        const vods =  await this.vodRepo.find({
            ...id? {where: {id}} : undefined,
            ...id? undefined : {
                take,
                skip
            },
            order: {
                id: "ASC"
            }
        });

        return Promise.all(vods.map(async (map) => {
            const contentsStatus = await this.getStatusByVODId(map.id);
            return {
                ...map,
                contentsStatus
            }
        }));
    }

    async getVODsCount(): Promise<number> {
        const total = await this.vodRepo.count();
        return Math.ceil(total/10);
    }
    async getVODsStatus(): Promise<any> {
        const [vods, total] = await this.vodRepo.findAndCount({
            where: {
                status: 1
            }
        });
        const err = [];
        for (const vod of vods) {
            const {id} = vod;
            const playlistKey = util.format(this.playlistKey, id);
            const redisPlaylistData = await this.redis.hgetall(playlistKey);
            let res = false;
            for (const redisPlaylist in redisPlaylistData) {
                res = true;
            }
            if (!res) {
                err.push(id);
            }
        }
        return {
            total,
            errTotal : err.length,
            err
        }
    }

    async getStatusByVODId(vid: number): Promise<VODStatusRes> {
        const {vod, vodQualities} = await this.getVODInfo(vid);

        const vodRes = new VODStatusRes();
        if (!vod) {
            vodRes.setResponse(false, [], false, []);
            return vodRes;
        }

        const subtitleData = await this.getVODSubtitle(vid);
        console.log(subtitleData);
        const subtitle = await subtitleData.map(subtitleOne => {
            return new VODSubtitleStatusRes()
                .setResponse(subtitleOne);
        });

        const {playTime, cid} = vod;
        const multiPlaylist = await this.redis.lrange(`vod:{${vid}}:multi`,0, -1);
        let multiPlaylistStatus = false;
        if (multiPlaylist.length > 0) {
            multiPlaylistStatus = true;
        }
        if (vodQualities.length < 1) {
            const qualityRes = new VODQualityStatusRes();
            qualityRes.setResponse(0, false);
            vodRes.setResponse(true, [qualityRes], multiPlaylistStatus, subtitle);
        } else {
            let vodQualityRes = [];
            for (const vodQuantity of vodQualities) {
                const qualityRes = new VODQualityStatusRes();
                const {quality, fps} = vodQuantity;
                const detailKey = `${quality}-${fps}`;
                const playlistKey = util.format(this.playlistKey, vid);
                const redisPlaylist = await this.redis.hget(playlistKey, detailKey);
                const s3Playlist = await this.getS3Playlist(quality, fps, vid, cid);

                const isS3 = !!s3Playlist;
                if (redisPlaylist) {
                    const tmpRedisPlaylist = redisPlaylist.replace(/\+/gi, "\n");
                    const playlistLength = tmpRedisPlaylist.length;
                    if (playTime/2+6 > playlistLength && playlistLength < 6) {
                        const tmpRes = qualityRes.setResponse(quality, true, false, isS3);
                        console.log(tmpRes);
                        vodQualityRes.push(tmpRes);
                    } else {
                        vodQualityRes.push(qualityRes.setResponse(quality, true, true, isS3));
                    }
                } else {
                    const tmpRes = qualityRes.setResponse(quality, true, false, isS3);
                    console.log(tmpRes);
                    vodQualityRes.push(tmpRes);
                }
            }
            vodRes.setResponse(true, vodQualityRes, multiPlaylistStatus, subtitle);
        }

        return vodRes;
    }
    async getConsumerIds(channelId: string, vid: string): Promise<string[]> {
        if (channelId.indexOf('PUBL') >= 0) {
            return [channelId];
        } else {
            let res = [];
            const key = `vod:channel:{PUBL-${channelId}-*}`;
            const consumerIds = await this.redis.keys(key);
            for (const consumerId of consumerIds) {
                const checkVODKey = `vod:channel:{${consumerId}`;
                const checkVOD = await this.redis.hget(checkVODKey, vid);
                if (checkVOD) {
                    res.push(consumerId);
                }
            }
            return res;
        }
    }

    async getVODToken(vid: number, query: GetVODTokenQuery): Promise<string> {
        const {consumerId} = query;
        const vod = await this.vodRepo.findOne({
            where: {
                id: vid
            }
        });
        if (!vod) {
            throw new HttpException('vod not found', 404);
        }

        const checkVODKey = `vod:channel:{${consumerId}`;
        const checkVOD = await this.redis.hget(checkVODKey, vid.toString());
        console.log(checkVOD);
        if (!checkVOD) {

        }

        return this.jwtService.signAsync(
            {
                id: this.distinctGenerator('TESTTOKEN'),
                cid: consumerId,
                consumerId,
                playVOD: vid,
            },
            {
                expiresIn: "10h",
            }
        );
    }

    distinctGenerator(prefix: string): string {
        const saltHead = Math.random().toString(36).substring(2,7);
        const timestampId = new Date().toISOString().replaceAll(/[\.\:\-TZ]/ig, '');
        const saltTail = Math.random().toString(36).substring(2,7);
        return `${prefix}${saltHead}${timestampId}-${saltTail}`;
    }

    async repairVODById(vid: number): Promise<VODStatusRes> {
        const {vod, vodQualities} = await this.getVODInfo(vid);

        const vodRes = new VODStatusRes();
        if (!vod) {
            vodRes.setResponse(false, [], false, []);
            return vodRes;
        }

        const subtitles = await this.vodSubtitleRepo.find({
            where: {
                vid,
                status: 1
            },
            order: {
                id: "DESC"
            }
        });

        const {playTime, cid, metadata} = vod;
        console.log(vodQualities);
        const multiPlaylist = ['#EXTM3U', '#EXT-X-VERSION:3'];
        const subtitleTag = (subtitles.length > 0)? this.subtitleInf : "";
        for (const subtitleData of subtitles) {
            const { language, isDefault, name } = subtitleData;

            const subtitleURI: string = util.format(this.subtitleUrl, vid, language);
            const defaultData: { [key: string]: string } = {
                true: "YES",
                false: "NO",
            };
            const defaultStr: string = defaultData[isDefault.toString()];
            const subtitleStr =
                '#EXT-X-MEDIA:TYPE=SUBTITLES,GROUP-ID="subs",NAME="%s",DEFAULT=%s,AUTOSELECT=NO,LANGUAGE="%s",URI="%s"';
            multiPlaylist.push(
                util.format(subtitleStr, name, defaultStr, language, subtitleURI)
            );
        }
        const bandwidthMaps = {
            "540": {
                "BANDWIDTH": 200000,
                "AVERAGE-BANDWIDTH": 700000,
            },
            "720": {
                "BANDWIDTH": 4000000,
                "AVERAGE-BANDWIDTH": 2000000,
            },
            "1080": {
                "BANDWIDTH": 6000000,
                "AVERAGE-BANDWIDTH": 4000000,
            }
        }
        const qualityTampKey = `#EXT-X-STREAM-INF:BANDWIDTH=%s,AVERAGE-BANDWIDTH=%s`;
        for(const vodQuality of vodQualities) {
            const {quality, fps} = vodQuality;
            let s3Playlist = await this.getS3Playlist(quality, fps, vid, cid);
            console.log(vodQuality);
            if (!s3Playlist) {
                if (quality === 540) {
                    const res = await this.checkLowQualityS3Playlist(quality, fps, vid, cid);
                    if (!res) {
                        throw new HttpException('s3 playlist not found', 404);
                    }
                }
                s3Playlist = await this.getS3Playlist(quality, fps, vid, cid);
                if (!s3Playlist) {
                    throw new HttpException('s3 playlist not found', 404);
                }
            }
            console.log(`s3 playlist`);
            // console.log(s3Playlist);
            const playlistLength = s3Playlist.split(/\n/).length;
            if (playTime/2+6 > playlistLength && playlistLength < 6) {
                throw new HttpException('s3 playlist invalid', 404);
            }
            const tmpPlaylist = s3Playlist.replace(/\n/gi, "+");
            let vodAPI = 'vod.publishingkit.net';
            if (this.nodeEnv === 'dev') {
                vodAPI = 'vod.dev.publishingkit.net';
            }
            let repairPlaylist = "";
            for (const partPlayList of s3Playlist.split(/\n/)) {
                if (partPlayList.indexOf('#EXT-X-KEY') > -1) {
                    const iv = JSON.parse(JSON.stringify(metadata)).iv;
                    repairPlaylist += `#EXT-X-KEY:METHOD=AES-128,URI="https://${vodAPI}/api/v1/key/${vid}",IV=0x${iv}+`;
                } else {
                    repairPlaylist += partPlayList+"+";
                }
            }
            console.log(repairPlaylist);
            const detailKey = `${quality}-${fps}`;
            const playlistTag = util.format(qualityTampKey,  bandwidthMaps[`${quality}`]["BANDWIDTH"], bandwidthMaps[`${quality}`]["AVERAGE-BANDWIDTH"]);
            multiPlaylist.push(playlistTag + subtitleTag);
            multiPlaylist.push(`${quality}/${fps}/playlist.m3u8`);
            const playlistKey = util.format(this.playlistKey, vid);
            await this.redis.hdel(playlistKey, detailKey);
            // await this.redis.hset(playlistKey, detailKey, tmpPlaylist);
            await this.redis.hset(playlistKey, detailKey, repairPlaylist);
        }
        // console.log(multiPlaylist);
        await this.redis.del(`vod:{${vid}}:multi`);
        for (const key of multiPlaylist) {
            await this.redis.rpush(`vod:{${vid}}:multi`, key);
        }
        const metaJson = JSON.parse(JSON.stringify(metadata));
        await this.redis.hset('vod:drm:key', `${vid}`, metaJson.key);

        const playUrl = `${this.playUrl}/${vid}/index.m3u8`;
        await this.redis.hset(`vod:channel:{${cid}}`, vid, playUrl);

        return await this.getStatusByVODId(vid);
    }

    async deduplicationVODById(vid: number): Promise<VODStatusRes> {
        const {vod, vodQualities} = await this.getVODInfo(vid);
        const validQuality = {
            1080: false,
            720: false,
            540: false
        };
        for (const vodQuality of vodQualities) {
            const {id, quality} = vodQuality;
            if (!validQuality[quality]) {
                validQuality[quality] = true;
            } else {
                await this.vodQualityRepo.delete({
                    id
                });
            }
        }

        return await this.getStatusByVODId(vid);
    }

    async getVODSubtitle(vid: number): Promise<VODSubtitleEntity[]> {
        return await this.vodSubtitleRepo.find({
            where: {
                vid,
            }
        });
    }

    async getVODInfo(vid: number) {
        const vod = await this.vodRepo.findOne({
            where: {
                id: vid
            }
        });

        const vodQualities = await this.vodQualityRepo.find({
            where: {
                vid,
                quality: Raw((quality) => `${quality} >= 540`),
            }
        });
        return {
            vod,
            vodQualities
        }
    }


    async getS3Playlist(quality: number, fps: number, id: number | string, cid?: string) {
        const key: string = util.format("%s/%s/%s/playlist.m3u8", id, quality, fps);
        try {
            const payload: aws.S3.Types.GetObjectOutput = await this.getData(key);
            const { Body } = payload;
            const playlist: string = Body.toString();
            return playlist;
        } catch (e) {
            if (cid) {
                return this.getS3Playlist(quality, fps, cid);
            } else {
                return null;
            }
        }
    }

    async setS3Playlist(quality: number, fps: number, id: number | string, playlist: string) {

    }

    async checkLowQualityS3Playlist(quality: number, fps: number, id: number | string, cid?: string) {
        console.log(`check low quality s3 `);
        const key1: string = util.format("%s/540/%s/", id, fps);
        const key2: string = util.format("%s/360/%s/", id, fps);

        console.log(key2);
        console.log(key1);
        try {
            await this.copyS3(key2, key1, id);
            return true;
        } catch (e) {
            console.log(e);
            return false;
        }
    }

    async copyS3(key: string, targetKey: string, id: number | string): Promise<void> {
        const data = await this.s3.listObjectsV2({
            Bucket: this.bucket,
            Prefix: key
        }).promise();

        for (const obj of data['Contents']) {
            const lockKey = obj.Key.replace(`${id}/360`, `${id}/540`);
            console.log(lockKey);
            await this.s3.copyObject({
                CopySource: `${this.bucket}/${obj.Key}`,
                Bucket: this.bucket,
                Key: lockKey
            }).promise();
        }
    }

    async setData(key: string, data: Buffer): Promise<void> {
        const params = {
            Bucket: this.bucket,
            Key: key,
            Body: data,
        };
        await this.s3.putObject(params).promise();
    }

    async getData(key: string): Promise<aws.S3.Types.GetObjectOutput> {
        const params = {
            Bucket: this.bucket,
            Key: key,
        };
        return await this.s3.getObject(params).promise();
    }
}
