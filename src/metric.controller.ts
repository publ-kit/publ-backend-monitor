import {Body, Controller, Get, Param, Post, Query} from '@nestjs/common';
import {AwsService} from "./aws.service";

@Controller('/metric')
export class MetricController {
    constructor(private readonly awsService: AwsService) {}

    @Get('/live/encoder')
    async getLiveEncoderMetric(
        @Query() query: GetLiveMetricBody
    ): Promise<GetLiveEncoderMetricRes[]> {
        return await this.awsService.getLiveEncoderMetric(query);
    }

    @Get('/live/relay')
    async getLiveRelayMetric(
        @Query() query: GetLiveMetricBody
    ): Promise<GetLiveRelayMetricRes> {
        return await this.awsService.getLiveRelayMetric(query);
    }

    @Get('/live/proxy')
    async getLiveProxyMetric(): Promise<GetLiveProxyMetricRes> {
        return await this.awsService.getLiveProxyMetric();
    }
}
