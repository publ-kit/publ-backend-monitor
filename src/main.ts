import {HttpAdapterHost, NestFactory} from '@nestjs/core';
import { AppModule } from './app.module';
import {
  FastifyAdapter,
  NestFastifyApplication
} from '@nestjs/platform-fastify';

async function bootstrap() {
  const appModule: object = AppModule;
  const app = await NestFactory.create<NestFastifyApplication>(
      appModule,
      new FastifyAdapter()
  );
  const {httpAdapter} = app.get<HttpAdapterHost>(HttpAdapterHost);
  app.setGlobalPrefix('/api/v1')
  app.enableCors({
    origin: "*",
    methods: ["GET", "POST", "OPTIONS"]
  });
  await app.listen(3300, "0.0.0.0");
}
bootstrap().then(()=>{});
