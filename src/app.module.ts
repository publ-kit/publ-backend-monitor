import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {
    LiveEncoderEntity,
    LiveEntity,
    PostgresqlModule,
    VODBatchJobEntity,
    VODEntity,
    VODQualityEntity,
    VODSubtitleEntity
} from "./db";
import {MetricController} from "./metric.controller";
import {AwsService} from "./aws.service";
import {VODService} from "./vod.service";
import {RedisModule} from "./db/redis";
import {ConfigModule, ConfigService} from "@nestjs/config";
import {JwtModule} from "@nestjs/jwt";
import {TypeOrmModule} from "@nestjs/typeorm";
import * as Repositories from './db/repositories';
import {LiveService} from "./live.service";

@Module({
    imports: [
        ConfigModule,
        PostgresqlModule.register(),
        RedisModule.register(),
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: (config: ConfigService) => ({
                secret: process.env.JWT_KEY
            }),
            inject: [ConfigService]
        })
    ],
    controllers: [AppController, MetricController],
    providers: [AppService, AwsService, VODService, LiveService, ...Object.values(Repositories)],
    exports: [ConfigModule]
})
export class AppModule {}
