import {Body, Controller, Get, Param, Post, Query} from '@nestjs/common';
import {AwsService} from "./aws.service";

@Controller('/status')
export class StatusController {
    constructor(private readonly awsService: AwsService) {}


    @Get('/live/api')
    async getLiveApi(): Promise<any> {
        return await this.awsService.getLiveProxyMetric();
    }
}
