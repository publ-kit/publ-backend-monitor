import {Injectable} from "@nestjs/common";
import {ConfigurationOptions} from "aws-sdk/lib/config-base";
import * as aws from "aws-sdk";


@Injectable()
export class AwsService {
    private readonly awsCloudWatch: aws.CloudWatch;
    private readonly awsEC2: aws.EC2;
    private readonly awsELB: aws.ELBv2;
    private readonly envType: string;
    constructor(
    ) {
        const ENV = process.env;
        this.envType = ENV.NODE_ENV;
        const accessKeyId: string = ENV.AWS_ACCESS;
        const secretAccessKey: string = ENV.AWS_SECRET;
        const awsOption: ConfigurationOptions = {
            accessKeyId,
            secretAccessKey,
            region: 'ap-northeast-2'
        };
        aws.config.update(awsOption);
        this.awsCloudWatch = new aws.CloudWatch();
        this.awsEC2 = new aws.EC2();
        this.awsELB = new aws.ELBv2();
    }

    // Live Encoder metric
    async getLiveEncoderMetric(body: GetLiveMetricBody): Promise<GetLiveEncoderMetricRes[]> {
        const {consumerId} = body;
        const param = {
            Filters: [{
                Name: "tag:Name",
                Values: [`*encoder-${consumerId}*`]
            }]
        };
        console.log(`*encoder-${consumerId}*`);
        const tagData = await this.awsEC2.describeTags(param).promise();

        if (tagData.Tags) {
            const {Tags} = tagData;
            const instances = [];
            for (const tag of Tags) {
                const instanceId = tag.ResourceId;
                const network = await this.getInstanceNetworkPackets(instanceId);
                const cpu = await this.getInstanceCPU(instanceId);
                instances.push({
                    name: tag.Value,
                    network,
                    cpu
                });
            }

            return instances;
        } else {
            return null;
        }
    }

    async getLiveRelayMetric(body: GetLiveMetricBody): Promise<GetLiveRelayMetricRes> {
        const {consumerId} =  body;
        const param = {
            Filters: [{
                Name: "tag:Name",
                Values: [`*relay-${consumerId}`]
            }]
        };
        console.log(param);
        const res = await this.awsEC2.describeTags(param).promise();
        if (res.Tags) {
            const {Tags} = res;
            let relay: GetLiveRelayMetricRes = null;
            for (const tag of Tags) {
                const instanceId = tag.ResourceId;
                const network = await this.getInstanceNetworkPackets(instanceId);
                relay = {
                    name: tag.Value,
                    network,
                };
            }
            return relay;
        } else {
            return null;
        }
    }

    async getLiveProxyMetric(): Promise<GetLiveProxyMetricRes> {
        const startTime = new Date();
        startTime.setHours(startTime.getHours()-1);
        let arn = 'targetgroup/awseb-AWSEB-1AJ7WPL8RPS5U/b563deb385175682';
        let autoScalGroup = 'awseb-e-cqtbnqptqg-stack-AWSEBAutoScalingGroup-1GCX0JFI28JSK';
        if (this.envType === 'dev') {
            arn = 'targetgroup/awseb-AWSEB-1XEA1VGH2YIC2/bc56f22ac553102f';
            autoScalGroup = 'awseb-e-pknqmm5cnd-stack-AWSEBAutoScalingGroup-1EZBLBZXGA3MX';
        }
        const error4xx = await this.getCloudWatch(
            'metric_alias_widget_6_0',
            [{
                Name: "TargetGroup",
                Value: arn
            }],
            'HTTPCode_Target_4XX_Count',
            'AWS/ApplicationELB',
            startTime,
            new Date(),
            'Sum'
        );
        const error5xx = await this.getCloudWatch(
            'metric_alias_widget_5_0',
            [{
                Name: "TargetGroup",
                Value: arn
            }],
            'HTTPCode_Target_5XX_Count',
            'AWS/ApplicationELB',
            startTime,
            new Date(),
            'Sum'
        );
        const cpu = await this.getCloudWatch(
            'cpu',
            [{
                Name: 'AutoScalingGroupName',
                Value: autoScalGroup
            }],
            'CPUUtilization',
            'AWS/EC2',
            startTime,
            new Date(),
            'Average'
        );

        return {
            error4xx,
            error5xx,
            cpu
        }
    }

    async getLiveApi(): Promise<any> {
        const startTime = new Date();
        startTime.setHours(startTime.getHours()-1);
        let dimensions = [{
            Name: 'LoadBalancer',
            Value: 'app/awseb-AWSEB-XD5JCEZSOZFR/8f4999dd7975d7aa'
        }];
        if (['dev','development'].includes(this.envType)) {
            dimensions = [{
                Name: 'LoadBalancer',
                Value: 'app/awseb-AWSEB-YLPY1GT64FMG/4d947bc3d3765562'
            }];
        }

        const error4xx = await this.getCloudWatch(
            'metric_alias_widget_6_0',
            dimensions,
            'HTTPCode_Target_4XX_Count',
            'AWS/ApplicationELB',
            startTime,
            new Date(),
            'Sum'
        );
        const error5xx = await this.getCloudWatch(
            'metric_alias_widget_5_0',
            dimensions,
            'HTTPCode_Target_5XX_Count',
            'AWS/ApplicationELB',
            startTime,
            new Date(),
            'Sum'
        );
        const request = await this.getCloudWatch(
            'metric_alias_widget_4_0',
            dimensions,
            'RequestCount',
            'AWS/ApplicationELB',
            startTime,
            new Date(),
            'Sum'
        );
        let status4xx = 'Green';
        let status5xx = 'Green';
        const [percent4xx, count4xx] = this.checkPercentage(request, error4xx);
        const [percent5xx, count5xx] = this.checkPercentage(request, error5xx);
        if (percent4xx > 10 && percent4xx < 50 && count4xx > 100) {
            status4xx = 'Yellow';
        } else if (percent4xx > 50 && count4xx > 100) {
            status4xx = 'Red';
        }
        if (percent5xx > 1 && count5xx > 1) {
            status5xx = 'Red';
        }

        return {
            status4xx,
            status5xx
        }
    }

    private checkPercentage(
        originData: aws.CloudWatch.Types.GetMetricDataOutput,
        checkData: aws.CloudWatch.Types.GetMetricDataOutput
    ): [number, number] {
        let total = 0;
        const originValues = originData.MetricDataResults.map((res)=> {
            return res.Values;
        });
        for (const value of originValues) {
            for (const val of value) {
                total += val;
            }
        }

        let check = 0;
        const checkValues = checkData.MetricDataResults.map((res)=> {
            return res.Values;
        })
        for (const value of checkValues) {
            for (const val of value) {
                check += val;
            }
        }

        if (check > 0 ){
            return [(check / total) * 100, check];
        } else {
            return [0,0];
        }
    }

    private async getInstanceError(arn: string) {

    }

    private async getInstanceNetworkPackets(instanceId: string): Promise<LiveEncoderNetworkPacketModel> {
        const startTime = new Date();
        startTime.setHours(startTime.getHours()-1);
        const input = await this.getCloudWatch(
            'network',
            [{
                Name: 'InstanceId',
                Value: instanceId
            }],
            "NetworkPacketsIn",
            "AWS/EC2",
            startTime,
            new Date(),
            "Sum"
        );
        const output = await this.getCloudWatch(
            'network',
            [{
                Name: 'InstanceId',
                Value: instanceId
            }],
            "NetworkPacketsOut",
            "AWS/EC2",
            startTime,
            new Date(),
            "Sum"
        );
        return {
            input,
            output
        }
    }

    private async getInstanceCPU(instanceId: string): Promise<any> {
        const startTime = new Date();
        startTime.setHours(startTime.getHours()-1);
        return await this.getCloudWatch(
            'cpu',
            [{
                Name: "InstanceId",
                Value: instanceId
            }],
            "CPUUtilization",
            "AWS/EC2",
            startTime,
            new Date(),
            "Maximum"
        );
    }

    private async getCloudWatch(
        id: string,
        dimensions: aws.CloudWatch.Types.Dimensions,
        metricName: string,
        nameSpace: string,
        startTime: Date,
        endTime: Date,
        stat: string, // "SampleCount"|"Average"|"Sum"|"Minimum"|"Maximum"
    ): Promise<aws.CloudWatch.Types.GetMetricDataOutput> {
        const params: aws.CloudWatch.Types.GetMetricDataInput = {
            EndTime: endTime,
            MetricDataQueries: [{
                Id: id,
                MetricStat: {
                    Metric: {
                        MetricName: metricName,
                        Namespace: nameSpace,
                        Dimensions: dimensions
                    },
                    Period: 60,
                    Stat: stat
                },
                ReturnData: true,
            }],
            StartTime: startTime
        };
        return this.awsCloudWatch.getMetricData(params).promise();
    }
}
