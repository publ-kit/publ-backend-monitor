import {DataSource, Repository} from "typeorm";
import {LiveEntity} from "../entities";
import {InjectDataSource} from "@nestjs/typeorm";

export class LiveRepository extends Repository<LiveEntity> {
    constructor(
        @InjectDataSource('live')
        private readonly dataSource: DataSource,
    ) {
        super(LiveEntity, dataSource.createEntityManager());
    }
}
