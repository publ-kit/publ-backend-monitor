import {LiveHistoryEntity} from "../entities";
import {DataSource, Repository} from "typeorm";
import {InjectDataSource} from "@nestjs/typeorm";

export class LiveHistoryRepository extends Repository<LiveHistoryEntity> {
    constructor(
        @InjectDataSource('live')
        private readonly dataSource: DataSource,
    ) {
        super(LiveHistoryEntity, dataSource.createEntityManager());
    }
}
