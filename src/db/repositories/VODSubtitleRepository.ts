import {DataSource, Repository} from "typeorm";
import {VODSubtitleEntity} from "../entities";
import {InjectDataSource} from "@nestjs/typeorm";

export class VODSubtitleRepository extends Repository<VODSubtitleEntity> {
    constructor(
        @InjectDataSource('vod')
        private readonly datasource: DataSource,
    ) {
        super(VODSubtitleEntity, datasource.createEntityManager());
    }
}
