import {DataSource, Repository} from "typeorm";
import {InjectDataSource} from "@nestjs/typeorm";
import {VODEntity} from "../entities";

export class VODRepository extends Repository<VODEntity> {
    constructor(
        @InjectDataSource('vod')
        private readonly dataSource: DataSource,
    ) {
        super(VODEntity, dataSource.createEntityManager());
    }
}
