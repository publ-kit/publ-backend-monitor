import {DataSource, Repository} from "typeorm";
import {VODQualityEntity} from "../entities";
import {InjectDataSource} from "@nestjs/typeorm";

export class VODQualityRepository extends Repository<VODQualityEntity> {
    constructor(
        @InjectDataSource('vod')
        private readonly dataSource: DataSource,
    ) {
        super(VODQualityEntity, dataSource.createEntityManager());
    }
}
