import {DataSource, Repository} from "typeorm";
import {VODBatchJobEntity} from "../entities";
import {InjectDataSource} from "@nestjs/typeorm";

export class VODBatchJobRepository extends Repository<VODBatchJobEntity> {
    constructor(
        @InjectDataSource('vod')
        private readonly dataSource: DataSource,
    ) {
        super(VODBatchJobEntity, dataSource.createEntityManager());
    }
}
