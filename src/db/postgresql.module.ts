import {DynamicModule, Module} from "@nestjs/common";
import {TypeOrmModule, TypeOrmModuleAsyncOptions} from "@nestjs/typeorm";
import {
    LiveEncoderEntity,
    LiveEntity,
    LiveHistoryEntity,
    VODBatchJobEntity,
    VODEntity,
    VODQualityEntity,
    VODSubtitleEntity
} from "./entities";
import {
    VODRepository,
    VODBatchJobRepository,
    VODQualityRepository,
    LiveRepository, LiveHistoryRepository
} from './repositories';
import * as Entities from './entities';


@Module({})
export class PostgresqlModule {
    static register(options: {
        connectionName?: string
    } = {}): DynamicModule {
        const vodEntities = [
            VODEntity, VODBatchJobEntity, VODQualityEntity, VODSubtitleEntity
        ];
        const liveEntities = [
            LiveEntity, LiveEncoderEntity, LiveHistoryEntity
        ];
        const typeOrmModuleOptions: TypeOrmModuleAsyncOptions = {
            imports: [],
            name: 'vod',
            useFactory: () => {
                const ENV_CONFIG = process.env.CONFIG;
                const dbOption = JSON.parse(ENV_CONFIG)["vod"];
                const ormOptions = {
                    name: 'vod',
                    ...dbOption,
                    entities: vodEntities,
                };
                return ormOptions;
            },
            inject: []
        };
        const typeOrmModuleOptions2: TypeOrmModuleAsyncOptions = {
            imports: [],
            name: 'live',
            useFactory: () => {
                const ENV_CONFIG = process.env.CONFIG;
                const dbOption = JSON.parse(ENV_CONFIG)["live"];
                const ormOptions = {
                    name: 'live',
                    ...dbOption,
                    entities: liveEntities,
                };
                return ormOptions;
            },
            inject: []
        };
        let imports = [];
        imports.push(TypeOrmModule.forRootAsync(typeOrmModuleOptions));
        imports.push(TypeOrmModule.forRootAsync(typeOrmModuleOptions2));
        // imports.push(TypeOrmModule.forFeature(vodEntities));
        // imports.push(TypeOrmModule.forFeature(liveEntities, 'live'));

        return {
            module: PostgresqlModule,
            imports,
            providers: [
                VODRepository,
                VODBatchJobRepository,
                VODQualityRepository,
                LiveRepository,
                LiveHistoryRepository,
            ],
            exports: [],
        }
    }
}
