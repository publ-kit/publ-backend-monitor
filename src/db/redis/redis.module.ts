/**
 * vod manager project redis module
 * 21.01
 */

import { DynamicModule, Module } from "@nestjs/common";
import {ConfigModule, ConfigService} from "@nestjs/config";
import { RedisService } from "./redis.service";
import {Redis} from "ioredis";
import * as process from "process";

@Module({})
export class RedisModule {
  static register(): DynamicModule {
    const RedisClientProvider = {
      provide: "REDIS_CLIENT",
      imports: [ConfigModule],
      useFactory: (config, logger) => {
        const redisOption = JSON.parse(process.env.ENV_REDIS);
        return new Redis(redisOption);
      },
      inject: [ConfigService],
    };
    return {
      module: RedisModule,
      imports: [ConfigModule],
      providers: [
        RedisClientProvider,
        RedisService,
      ],
      exports: ["REDIS_CLIENT", RedisService],
    };
  }
}
