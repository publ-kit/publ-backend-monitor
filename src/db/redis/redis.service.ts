

import {Inject, Injectable, OnApplicationBootstrap, OnModuleInit} from "@nestjs/common";
import {Redis, Cluster} from 'ioredis';

@Injectable()
export class RedisService implements  OnApplicationBootstrap, OnModuleInit {

  @Inject('REDIS_CLIENT')
  readonly client: Redis & Cluster;
  // readonly client: IORedis.Redis;

  onApplicationBootstrap(): any {
    this.addClientListener();
  }

  onModuleInit(): any {
    this.addClientListener();
  }

  private addClientListener() {

  }
}
