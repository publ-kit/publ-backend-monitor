import {Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";

@Entity({database: 'live', name:'LiveEncoder'})
export class LiveEncoderEntity {
    @PrimaryGeneratedColumn({
        type: "bigint",
        name: "id"
    })
    id: number;

    @Column({
        type: "bigint",
        name: "lid",
        default: null,
    })
    lid: number;

    @Column({
        type: "varchar",
        name: "status",
        default: "NONE",
        comment: "NONE, INIT, ON_STREAM, OUT_STREAM"
    })
    status: string;

    @Column({
        type: "varchar",
        name: "instanceId",
        default: null
    })
    instanceId: string;

    @Column({
        type: "varchar",
        name: "ipAddr",
        default: null
    })
    ipAddr: string;

    @CreateDateColumn({
        type: "timestamptz",
        name: "createdAt",
        nullable: false,
    })
    createdAt: string;

    @UpdateDateColumn({
        type: "timestamptz",
        name: "updatedAt",
        nullable: false
    })
    updatedAt: string;
}
