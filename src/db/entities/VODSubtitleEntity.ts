import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity("VODSubtitle")
@Index(["vid"])
@Index(["isDefault"])
export class VODSubtitleEntity {
  @PrimaryGeneratedColumn({
    type: "bigint",
    name: "id",
  })
  id: number;

  @Column({
    type: "bigint",
    name: "vid",
  })
  vid: number;

  @Column({
    type: "smallint",
    name: "status",
    nullable: false,
    default: 0,
    enum: [0, 1, 2],
  })
  status: number;

  @Column({
    type: "varchar",
    name: "code",
    nullable: true,
    default: null,
  })
  code: string;

  @Column({
    type: "varchar",
    name: "subtitleSrc",
    default: null,
  })
  subtitleSrc: string;

  @Column({
    type: "varchar",
    name: "subtitleUrl",
  })
  subtitleUrl: string;

  @Column({
    type: "varchar",
    name: "name",
  })
  name: string;

  @Column({
    type: "varchar",
    name: "language",
  })
  language: string;

  @Column({
    type: "boolean",
    name: "isDefault",
  })
  isDefault: boolean;

  @CreateDateColumn({
    type: "timestamptz",
    name: "createdAt",
    nullable: false,
  })
  createdAt: string;

  @UpdateDateColumn({
    type: "timestamptz",
    name: "updatedAt",
    nullable: false,
  })
  updatedAt: string;
}

/**
-- public.vod_play definition

-- Drop table

-- DROP TABLE public.vod_play;

CREATE TABLE public.vod_play (
	id bigserial NOT NULL,
	cid varchar NOT NULL,
	"userId" int4 NOT NULL,
	"seriesId" int4 NOT NULL,
	"episodeId" int4 NOT NULL,
	"postId" int4 NOT NULL,
	"nextId" int4 NOT NULL,
	"playTime" int4 NOT NULL,
	"insertedAt" timestamp NOT NULL DEFAULT now(),
	"updatedAt" timestamp NOT NULL DEFAULT now(),
	CONSTRAINT "PK_bd5218718e9bfa8b79a2a7d7473" PRIMARY KEY (id)
);
CREATE INDEX "IDX_122b0e1043ab17084df3aa2eff" ON public.vod_play USING btree (cid);
 */
