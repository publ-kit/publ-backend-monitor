/**
 * vod project entity
 * 21.04.26
 */

import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity("vod_batch_job")
@Index("jobId", ["jobId"])
export class VODBatchJobEntity {
  @PrimaryGeneratedColumn({
    type: "bigint",
    name: "id",
  })
  id: number;

  @Column({
    type: "int",
    name: "qId",
    nullable: true,
  })
  qId: number;

  @Column({
    type: "varchar",
    name: "jobId",
    nullable: false,
  })
  jobId: string;

  @Column({
    type: "varchar",
    name: "pid",
    nullable: false,
  })
  pid: string;

  @CreateDateColumn({
    type: "timestamp",
    name: "createdAt",
    nullable: false,
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: "timestamp",
    name: "updatedAt",
    nullable: false,
  })
  updatedAt: Date;
}
