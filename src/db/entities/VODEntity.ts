/**
 * vod project entity
 * 21.04.26
 */

import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity('VOD')
@Index(["cid"])
@Index(["status"])
export class VODEntity {
  static publicListSelect: string[] = ["id", "playTime", "createdAt"];
  static publicSelect: string[] = ["id", "playUrl"];
  static privateSelect: string[] = [
    "id",
    "status",
    "playTime",
    "playUrl",
    "contentUrl",
    "metadata",
    "createdAt",
    "updatedAt",
  ];

  @PrimaryGeneratedColumn({
    type: "bigint",
    name: "id",
  })
  id: number;

  @Column({
    type: "varchar",
    name: "cid",
    nullable: false,
  })
  cid: string;

  @Column({
    type: "varchar",
    name: "pid",
    nullable: false,
  })
  pid: string;

  @Column({
    type: "smallint",
    name: "status",
    nullable: false,
    default: 0,
    enum: [0, 1, 2],
  })
  status: number;

  @Column({
    type: "varchar",
    name: "code",
    nullable: true,
    default: null,
  })
  code: string;

  @Column({
    type: "int4",
    name: "playTime",
  })
  playTime: number;

  @Column({
    type: "varchar",
    name: "playUrl",
  })
  playUrl: string;

  @Column({
    type: "varchar",
    name: "contentUrl",
  })
  contentUrl: string;

  @Column({
    type: "json",
    name: "metadata",
  })
  metadata: { [key: string]: string };

  @CreateDateColumn({
    type: "timestamp",
    name: "createdAt",
    nullable: false,
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: "timestamp",
    name: "updatedAt",
    nullable: false,
  })
  updatedAt: Date;
}
