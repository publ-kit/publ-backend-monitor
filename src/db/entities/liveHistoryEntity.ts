import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, Unique, UpdateDateColumn } from "typeorm";

export class EncoderMetaDataModel {
    key: string;
    iv: string;
}

@Entity('LiveHistory')
export class LiveHistoryEntity {
    @PrimaryGeneratedColumn({
        type: "bigint",
        name: "id",
    })
    id: number;

    @Column({
        type: "varchar",
        name: "cid",
    })
    cid: string;

    @Column({
        type: "bigint",
        name: "lid",
    })
    lid: number;

    @Column({
        type: "varchar",
        name: "title",
        nullable: false
    })
    title: string;

    @Column({
        type: "varchar",
        name: "thumbnailUrl",
        nullable: false,
        default: 'https://publ-sw-core-image.s3.ap-northeast-2.amazonaws.com/broadcast_standby.png'
    })
    thumbnailUrl: string;

    @Column({
        type: "bigint",
        name: "totalViewer",
        default: 0,
    })
    totalViewer: number;

    @Column({
        type: "bigint",
        name: "concurrentViewer",
        default: 0,
    })
    concurrentViewer: number;

    @Column({
        type: "varchar",
        name: "streamKey",
        nullable: false
    })
    streamKey: string;

    @Column({
        type: "varchar",
        name: "status",
        default: "NONE",
        comment: "NONE, TERMINATED, CANCELED"
    })
    status: string;

    @Column({
        type: "json",
        name: "metaData",
        nullable: false
    })
    metaData: EncoderMetaDataModel;

    @Column({
        type: "timestamptz",
        name: "startTime",
        nullable: false
    })
    startTime: string;

    @Column({
        type: "timestamptz",
        name: "endTime",
        nullable: false
    })
    endTime: string;

    @CreateDateColumn({
        type: "timestamptz",
        name: "createdAt",
        nullable: false,
    })
    createdAt: string;
}
