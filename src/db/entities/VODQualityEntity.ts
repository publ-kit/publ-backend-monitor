import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity("VODQuality")
export class VODQualityEntity {
  @PrimaryGeneratedColumn({
    type: "bigint",
    name: "id",
  })
  id: number;

  @Column({
    type: "bigint",
    name: "vid",
  })
  vid: number;

  @Column({
    type: "int",
    name: "quality",
    nullable: false,
    default: 360,
  })
  quality: number;

  @Column({
    type: "int",
    name: "fps",
    nullable: false,
    default: 30,
  })
  fps: number;

  @Column({
    type: "int",
    name: "status",
    nullable: false,
    default: 0,
  })
  status: number;

  @Column({
    type: "varchar",
    name: "code",
    nullable: true,
    default: null,
  })
  code: string;

  @CreateDateColumn({
    type: "timestamptz",
    name: "createdAt",
    nullable: false,
  })
  createdAt: string;

  @UpdateDateColumn({
    type: "timestamptz",
    name: "updatedAt",
    nullable: false,
  })
  updatedAt: string;
}
