import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';

@Entity({database: 'live', name: 'Live'})
export class LiveEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
  })
  id: number;

  @Column({
    type: 'varchar',
    name: 'cid',
  })
  cid: string;

  @Column({
    type: 'varchar',
    name: 'title',
    nullable: false,
  })
  title: string;

  @Column({
    type: 'varchar',
    name: 'thumbnailUrl',
    nullable: false,
    default:
      'https://publ-sw-core-image.s3.ap-northeast-2.amazonaws.com/broadcast_standby.png',
  })
  thumbnailUrl: string;

  @Column({
    type: 'varchar',
    name: 'streamKey',
    nullable: false,
  })
  streamKey: string;

  @Column({
    type: 'varchar',
    name: 'status',
    default: 'NONE',
    comment: 'NONE, PUBLISHED, STAND_BY, ON_AIR, TERMINATED, CANCELED',
  })
  status: string;

  @Column({
    type: 'json',
    name: 'metaData',
    nullable: false,
  })
  metaData: any;

  @Column({
    type: 'timestamptz',
    name: 'startTime',
    nullable: false,
  })
  startTime: string;

  @Column({
    type: 'timestamptz',
    name: 'endTime',
    nullable: false,
  })
  endTime: string;

  @Column({
    type: 'varchar',
    name: 'mode',
    default: 'LANDSCAPE',
    enum: ['LANDSCAPE', 'VERTICAL'],
  })
  mode: string;

  @CreateDateColumn({
    type: 'timestamptz',
    name: 'createdAt',
    nullable: false,
  })
  createdAt: string;

  @UpdateDateColumn({
    type: 'timestamptz',
    name: 'updatedAt',
    nullable: false,
  })
  updatedAt: string;
}
