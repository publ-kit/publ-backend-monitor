import {Injectable} from "@nestjs/common";
import {PagingModel} from "./vod.service";
import {LiveHistoryRepository} from "./db";
import * as aws from "aws-sdk";
import * as util from "util";
import {LiveHistoriesResModel} from "./app.controller";
import * as amqp from "amqplib";
import {Options} from "amqplib/properties";
import {ListObjectsV2Output, ObjectList} from "aws-sdk/clients/s3";

class RecordedVideo {
    videoUrl: string;
}
@Injectable()
export class LiveService {
    bucket: string;
    s3: aws.S3;
    queue: string;
    private readonly mqOption: Options.Connect;
    constructor(
        private readonly liveHistoryRepo: LiveHistoryRepository,
    ) {
        const env = process.env;
        this.bucket = env.BUCKET;
        aws.config.update({
            credentials: {
                accessKeyId: env.AWS_ACCESS,
                secretAccessKey: env.AWS_SECRET
            },
            region: env.AWS_REGION
        });
        this.s3 = new aws.S3();
        this.mqOption = {
            protocol: env.MQ_PRO,
            hostname: env.MQ_HOST,
            port: Number(env.MQ_PORT),
            password: env.MQ_PASS,
            username: env.MQ_USER
        };
        this.queue = env.MQ_RECORD_CHANNEL;
    }

    async getLiveHistories(paging: PagingModel): Promise<LiveHistoriesResModel[]> {
        const {page, size, id} = paging;
        const take =  size? size : 10;
        const skip = size * Number(page? page - 1 : 0);

        const liveHistories = await this.liveHistoryRepo.find({
            ...id? {where: {id}} : undefined,
            ...id? undefined : {
                take,
                skip
            },
            order: {
                id: "ASC"
            }
        });

        return await Promise.all(liveHistories.map(async(liveHistory)=>{
            const {streamKey, lid} = liveHistory;
            const signedUrl = await this.getS3LiveRecord(streamKey, lid);
            const recordStatus = !!(signedUrl);
            return {
                ...liveHistory,
                recordStatus,
                signedUrl
            }
        }));
    }

    async requestLiveHistoryRecord(query: {cid: string, lid: number}): Promise<boolean> {
        const liveHistory = await this.liveHistoryRepo.findOne({
            where: {
                lid: query.lid
            }
        });
        const {streamKey,startTime, endTime} = liveHistory;
        const checkReq = await this.checkLiveTs(
            streamKey,
            1080,
            new Date(startTime).getTime(),
            new Date(endTime).getTime()
        );
        if (checkReq.length > 0) {
            await this.sendQueue(query);
        } else {
            return false;
        }

        return true;
    }

    async getRecordedVideos(streamKey: string, lid: string): Promise<RecordedVideo[]> {
       const res = await this.getRecordedVideosFromS3(`live/${streamKey}/${lid}`);

       return res.map((object)=> {
           const {Key} = object;
           return {
               videoUrl : `https://s3.ap-northeast-2.amazonaws.com/${this.bucket}/${Key}`
           };
       });
    }

    private async getS3LiveRecord(streamKey: string, lid: number) {
        const key = util.format(`live/${streamKey}/${lid}.mp4`)
        try {
            const payload: aws.S3.Types.GetObjectOutput = await this.getData(key);
            const signedUrl = `https://s3.ap-northeast-2.amazonaws.com/${this.bucket}/live/${encodeURIComponent(streamKey)}/${lid}.mp4`;
            return signedUrl;
        } catch (e) {
            return null;
        }
    }

    private async getData(key: string): Promise<aws.S3.Types.GetObjectOutput> {
        const params = {
            Bucket: this.bucket,
            Key: key,
        };
        return await this.s3.headObject(params).promise();
        // return await this.s3.getObject(params).promise();
    }

    private async checkLiveTs(
        streamKey: string,
        quality: number,
        start: number,
        end: number,
    ): Promise<aws.S3.Types.ObjectList> {
        const contents = await this.getRecordTargets(streamKey, quality, start-900);
        if (contents.length < 1) return [];
        let lastModified = contents[contents.length-1].LastModified;
        let loop = true;
        let checkModified = new Date();
        while (loop) {
            const targetTime = lastModified;
            const tmpContents = await this.getRecordTargets(streamKey, quality, new Date(targetTime).getTime() / 1000);
            contents.push(...tmpContents);
            lastModified = contents[contents.length - 1].LastModified;
            if (checkModified === lastModified) {
                loop = false;
                break;
            }
            checkModified = lastModified;
            if (new Date(lastModified) > new Date(end)) {
                loop = false;
                break;
            }
        }
        return contents;
    }

    private async getRecordedVideosFromS3(prefix: string): Promise<aws.S3.Types.ObjectList> {
        console.log(prefix);
        const param: aws.S3.Types.ListObjectsV2Request = {
            Bucket: this.bucket,
            Prefix: prefix,
            MaxKeys: 1000
        };
        const res = await this.s3.listObjectsV2(param).promise();
        return res.Contents;
    }

    private async getRecordTargets(streamKey: string, quality: number, time: number) {
        const param: aws.S3.Types.ListObjectsV2Request = {
            Bucket: this.bucket,
            Prefix: `live/${streamKey}/${quality}`,
            StartAfter: `live/${streamKey}/${quality}/${time}.ts`,
            MaxKeys: 1000
        };
        const res = await this.s3.listObjectsV2(param).promise();
        return res.Contents;
    }

    private async sendQueue(payload) {
        const buffer = Buffer.from(JSON.stringify(payload));

        const con = await amqp.connect(this.mqOption);
        const channel = await con.createChannel();
        await channel.prefetch(1);
        const queue = await channel.assertQueue(
            this.queue,
            {
                durable: true
            }
        );

        return channel.sendToQueue(this.queue, buffer);
    }
}
