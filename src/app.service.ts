import { Injectable } from '@nestjs/common';
import * as aws from 'aws-sdk';
import * as util from 'util';
import {ConfigurationOptions} from 'aws-sdk/lib/config-base';
import * as amqp from 'amqplib';
import {Options} from 'amqplib/properties'
import {InjectDataSource, InjectRepository} from "@nestjs/typeorm";
import {LiveEntity, LiveRepository, VODBatchJobEntity, VODBatchJobRepository, VODQualityEntity} from "./db";
import {In, Repository, DataSource} from "typeorm";
import jwt from "jsonwebtoken"
import {VODQualityRepository} from "./db/repositories";

interface EncodingQualityData {
    qId: number;
    quality: number;
    jobId: string;
    logStreamName: string;
    jobLogStreams: string[];
}

interface EncodingJobResModel {
    uuid: string;
    vid: number;
    qualities: EncodingQualityData[];
}

@Injectable()
export class AppService {
    private readonly batch: aws.Batch;
    private readonly cw: aws.CloudWatchLogs;
    private readonly monitoring: aws.CloudWatch;
    private readonly envType: string;
    private readonly mqOption: Options.Connect;
    private readonly queue: string;
    private readonly workQueue: string;
    private readonly streamUrl: string;
    private readonly adminUsers: string[];
    constructor(
        private readonly vodQualityRepo: VODQualityRepository,
        private readonly vodBatchJobRepo: VODBatchJobRepository,
        private readonly liveRepo: LiveRepository,
        // @InjectDataSource('live')
        // private readonly liveDataSource: DataSource,
    ) {
        const ENV = process.env;
        this.envType = ENV.NODE_ENV;
        const accessKeyId: string = ENV.AWS_ACCESS;
        const secretAccessKey: string = ENV.AWS_SECRET;
        const awsOption: ConfigurationOptions = {
            accessKeyId,
            secretAccessKey,
            region: 'ap-northeast-2'
        };
        aws.config.update(awsOption);
        this.batch = new aws.Batch();
        this.cw = new aws.CloudWatchLogs();
        this.monitoring = new aws.CloudWatch();
        this.adminUsers = JSON.parse(ENV.ADMIN_USERS);

        this.mqOption = {
            protocol: ENV.MQ_PRO,
            hostname: ENV.MQ_HOST,
            port: Number(ENV.MQ_PORT),
            password: ENV.MQ_PASS,
            username: ENV.MQ_USER
        };
        this.queue = ENV.MQ_NAME;
        this.workQueue = ENV.MQ_WORK;
        this.streamUrl = ENV.STREAM_URL;
    }

    getTargetMap(target: string): any {
        if (this.envType === "prod") {
            switch (target) {
                case "VOD-PROXY":
                    return [
                        {
                            Name: "TargetGroup",
                            Value: "targetgroup/awseb-AWSEB-1WCYCIDOBAQ9O/b23cb1b1871e2a13"
                        },
                        {
                            Name: "LoadBalancer",
                            Value: "app/awseb-AWSEB-SIZNHWOP8IBO/704ba01fb6507493"
                        }
                    ];
                    break;
                case "VOD-USER":
                    return [
                        {
                            Name: "TargetGroup",
                            Value: "targetgroup/awseb-AWSEB-2EPFBHID1LZZ/f370cfdce26a7556"
                        },
                        {
                            Name: "LoadBalancer",
                            Value: "app/awseb-AWSEB-LV6U1V63ZEGJ/898c8d380bcad77e"
                        }
                    ];
                    break;
                case "VOD-SELLER":
                    return [
                        {
                            Name: "TargetGroup",
                            Value: "targetgroup/awseb-AWSEB-15YHQITA77TNC/c43389a19dfdbcfa"
                        },
                        {
                            Name: "LoadBalancer",
                            Value: "app/awseb-AWSEB-15YVEY0FU1FCJ/9a43e7b1a31a4395"
                        }
                    ];
                    break;
                case "LIVE-PROXY":
                    return [
                        {
                            Name: "TargetGroup",
                            Value: "targetgroup/awseb-AWSEB-1AJ7WPL8RPS5U/b563deb385175682"
                        },
                        {
                            Name: "LoadBalancer",
                            Value: "app/awseb-AWSEB-XD5JCEZSOZFR/8f4999dd7975d7aa"
                        }
                    ];
                    break;
                case "LIVE-USER":
                    return [
                        {
                            Name: "TargetGroup",
                            Value: "targetgroup/awseb-AWSEB-1BW2683Q5H3CT/71df99bf602f5b6d"
                        },
                        {
                            Name: "LoadBalancer",
                            Value: "app/awseb-AWSEB-18O9E7KI7I0FZ/4f44fb00be0c410b"
                        }
                    ];
                    break;
                case "LIVE-SELLER":
                    return [
                        {
                            Name: "TargetGroup",
                            Value: "targetgroup/awseb-AWSEB-RCWPQ1J4JQRV/47025543bdaf6431"
                        },
                        {
                            Name: "LoadBalancer",
                            Value: "app/awseb-AWSEB-150B7W5CLME4Y/47d7e3a469998acb"
                        }
                    ];
                    break;
            }
        } else {
            switch (target) {
                case "VOD-PROXY":
                    return [
                        {
                            Name: "TargetGroup",
                            Value: "targetgroup/awseb-AWSEB-FTUTSV8FPJLC/599f9450f95a2a04"
                        },
                        {
                            Name: "LoadBalancer",
                            Value: "app/awseb-AWSEB-VZSM22NU3JYQ/91033e9051943a20"
                        }
                    ];
                    break;
                case "VOD-USER":
                    return [
                        {
                            Name: "TargetGroup",
                            Value: "targetgroup/awseb-AWSEB-T5DAE1861U8U/5ea3721d059e5622"
                        },
                        {
                            Name: "LoadBalancer",
                            Value: "app/awseb-AWSEB-LSF9O35O9BKQ/a29d4ec5385f3881"
                        }
                    ];
                    break;
                case "VOD-SELLER":
                    return [
                        {
                            Name: "TargetGroup",
                            Value: "targetgroup/awseb-AWSEB-D0WBMGHTQHTH/d512c3ba03f957ea"
                        },
                        {
                            Name: "LoadBalancer",
                            Value: "app/awseb-AWSEB-V2WGPVHNI6MS/4641793b7967a76c"
                        }
                    ];
                    break;
                case "LIVE-PROXY":
                    return [
                        {
                            Name: "TargetGroup",
                            Value: "targetgroup/awseb-AWSEB-1XEA1VGH2YIC2/bc56f22ac553102f"
                        },
                        {
                            Name: "LoadBalancer",
                            Value: "app/awseb-AWSEB-YLPY1GT64FMG/4d947bc3d3765562"
                        }
                    ];
                    break;
                case "LIVE-USER":
                    return [
                        {
                            Name: "TargetGroup",
                            Value: "targetgroup/awseb-AWSEB-YD5GRXL0DBD0/01b510c61f11ad60"
                        },
                        {
                            Name: "LoadBalancer",
                            Value: "app/awseb-AWSEB-1F7ZNGJGEICXU/2ce01a3264d96c7c"
                        }
                    ];
                    break;
                case "LIVE-SELLER":
                    return [
                        {
                            Name: "TargetGroup",
                            Value: "targetgroup/awseb-AWSEB-RPMURR9KB3GO/c1c467083530eb5d"
                        },
                        {
                            Name: "LoadBalancer",
                            Value: "app/awseb-AWSEB-1RP0NDQWNDMY8/f9d3f4c05001d941"
                        }
                    ];
                    break;
            }
        }
    }

    getMetricStat(metricName: string): string {
        switch (metricName) {
            case "TargetResponseTime":
                return "Average";
                break;
            case "RequestCount":
                return "Sum";
                break;
            case "HTTPCode_Target_5XX_Count":
                return "Sum";
                break;
            case "HTTPCode_Target_4XX_Count":
                return "Sum";
                break;
        }
    }

    async getVODLogStream(query): Promise<any> {
        const {startTime, endTime, metricName, target} = query;
        let Dimensions = this.getTargetMap(target);

        let EndTime = new Date(endTime);
        let StartTime = new Date(startTime);
        const logs = await this.monitoring.getMetricData({
            StartTime,
            EndTime,
            MetricDataQueries: [
                {
                    Id: "metric_alias_widget",
                    MetricStat: {
                        Metric: {
                            Namespace: "AWS/ApplicationELB",
                            MetricName: metricName,
                            Dimensions
                        },
                        Stat: this.getMetricStat(metricName),
                        Period: 60
                    }
                }
            ]
        }).promise();
        // console.log(logs.MetricDataResults);
        return logs;
    }

    async getEncodingsLog(uuid: string): Promise<EncodingJobResModel> {
        const batchJobs = await this.vodBatchJobRepo.find({
            where: {
                pid: uuid
            }
        });
        const ids = [];
        const jobs = [];
        const jobLogData = {};
        for (const job of batchJobs) {
            const { qId, jobId } = job;
            ids.push(qId);
            jobs.push(jobId);
            jobLogData[jobId] = {
                qId,
                jobId
            };
        }
        const qualityData = await this.getQualityDataByIds(ids);
        let vid = null;
        const qualities = [];
        const qualityJobData = {};
        for (const quality of qualityData) {
            vid = quality.vid;
            qualityJobData[quality.id] = quality.quality;
        }
        const param = {
            jobs
        };
        const batchJob = await this.batch.describeJobs(param).promise();
        for (const job of batchJob.jobs) {
            const {container, jobId} = job;
            const {logStreamName} = container;
            const {messages} = await this.getLogStream(logStreamName, 10);
            jobLogData[jobId].quality = qualityJobData[jobLogData[jobId].qId];
            jobLogData[jobId].logStreamName = logStreamName;
            jobLogData[jobId].jobLogStreams = messages;
            qualities.push(jobLogData[jobId]);
        }
        return {
            uuid,
            vid,
            qualities
        }
    }

    private async getQualityDataById(id: number): Promise<VODQualityEntity> {
        return this.vodQualityRepo.findOne({
            where: {
                id
            }
        });
    }

    private async getQualityDataByIds(ids: number[]): Promise<VODQualityEntity[]> {
        return this.vodQualityRepo.find({
            where: {
                id: In(ids)
            }
        });
    }

    private async getLogStreamByStreamName(logStreamName: string): Promise<any> {
        const logs: aws.CloudWatchLogs.Types.GetLogEventsResponse = await this.cw.getLogEvents({
            logGroupName: '/aws/batch/job',
            logStreamName,
            limit: 100
        }).promise();
    }

    async getLogStream(logStreamName: string, limit?: number, nextToken?: string): Promise<any> {
        const logGroupName = '/aws/batch/job';
        const logs: aws.CloudWatchLogs.Types.GetLogEventsResponse = await this.cw.getLogEvents({
            logGroupName,
            logStreamName,
            nextToken,
            startFromHead: false,
            limit: limit? limit: 500,
        }).promise();

        let messages: string[] = [];

        const {events, nextBackwardToken} = logs;
        for (let event of events) {
            const {message} = event;
            messages.push(message);
        }

        return {
            messages,
            nextBackwardToken
        };
    }

    async getEncodeJobList(
        status?: "SUBMITTED"|"PENDING"|"RUNNABLE"|"STARTING"|"RUNNING"|"SUCCEEDED"|"FAILED"|string
    ): Promise<any[]> {
        const qualityList: string[] = ['360', '480', '540', '720', '1080', 'sub', 'simple'];
        let res: any[] = [];
        let jobQueueNameForm: string = null;
        if (this.envType === 'dev') {
            jobQueueNameForm = "publ-encoder-dev-job-queue%s";
        } else {
            jobQueueNameForm = `publ-${this.envType}-encoder-job-queue%s`;
        }

        let checkJobs: string[] = [];
        for (let quality of qualityList) {
            const jobQueue: string = util.format(jobQueueNameForm, `-${quality}`);
            const batchJob: aws.Batch.Types.ListJobsResponse = await this.batch.listJobs({
                jobQueue,
                jobStatus: status||"RUNNING",
                maxResults: 25
            }).promise();
            const {jobSummaryList} = batchJob;
            for(let summary of jobSummaryList) {
                const { jobId} = summary;
                checkJobs.push(jobId);
            }
        }
        if (checkJobs.length > 0) {
           res = await this.getJobDescribe(checkJobs);
        }

        return res;
    }

    async doSync(data): Promise<boolean> {
        const {cid, liveId, bStatus, sStatus, startTime} = data;
        const liveData = await this.getLive(liveId);
        const payload = {
            cid,
            id: liveId,
            broadcastStatus: bStatus,
            streamStatus: sStatus,
            thumbnailUrl: liveData.thumbnailUrl,
            streamUrl: `${this.streamUrl}/${liveId}/index.m3u8`,
            startTime
        };
        const buffer = Buffer.from(JSON.stringify(payload));

        const con = await amqp.connect(this.mqOption);
        const channel = await con.createChannel();
        await channel.prefetch(1);
        const queue = await channel.assertQueue(
            this.queue,
            {
                durable: true
            }
        );

        return channel.sendToQueue(this.queue, buffer);
    }

    async doDestroy(cid: string): Promise<boolean> {
        const payload = {
            cid,
            type: "DESTROY"
        };
        console.log(payload);
        const buffer = Buffer.from(JSON.stringify(payload));

        const con = await amqp.connect(this.mqOption);
        const channel = await con.createChannel();
        await channel.prefetch(1);
        const queue = await channel.assertQueue(
            this.workQueue,
            {
                durable: true
            }
        );

        return channel.sendToQueue(this.workQueue, buffer);
    }

    async getLogin(host: string): Promise<string> {
        let clientId = '';
        let clientSec = '';
        if (host) {
            if (host === 'monit.dev.publishingkit.net') {
                clientId = '1481436478804.4558936185286';
            } else if (host === 'monit.publishingkit.net') {
                clientId = '1481436478804.4558957911750';
            }
        } else {
            clientId = '1481436478804.4558936185286';
        }

        return `https://slack.com/oauth/v2/authorize?user_scope=identity.basic&client_id=${clientId}`;
    }

    async getAUTHUrl(host: string, code): Promise<string> {
        let clientId = '';
        let clientSec = '';
        if (host) {
            if (host === 'monit.dev.publishingkit.net') {
                clientId = '1481436478804.4558936185286';
                clientSec = '8137e3e80fe445f4b0969eebc1953f5c';
            } else if (host === 'monit.publishingkit.net') {
                clientId = '1481436478804.4558957911750';
                clientSec = '19996c8568882b43c15f8c48f37bf0a5';
            }
        } else {
            clientId = '1481436478804.4558957911750';
            clientSec = '19996c8568882b43c15f8c48f37bf0a5';
        }
        return `https://slack.com/api/oauth.v2.access?client_id=${clientId}&client_secret=${clientSec}&code=${code}`;
    }

    async getBatchLogsByJobId(jobId: string, nextBackwardToken?: string): Promise<any> {
        // const jobs = await this.getJobDescribe([jobId]);
        const jobDescribe: aws.Batch.Types.DescribeJobsResponse = await this.batch.describeJobs({
            jobs:[jobId]
        }).promise();
        let {jobs} = jobDescribe;
        jobs.sort(function (jobA, jobB) {
            if (jobA.startedAt < jobB.startedAt) return 1;
            if (jobA.startedAt > jobB.startedAt) return -1;
            return 0;
        });
        let res = {};
        for (const jobDetail of jobs) {
            const {container, jobId, startedAt} = jobDetail;
            const {logStreamName} = container;
            console.log(nextBackwardToken);
            res = await this.getLogStream(logStreamName, 500, (nextBackwardToken!=="null")? nextBackwardToken: undefined);
        }
        return res;
    }

    async getBatchJobQueuesByName(queueName: string, status: string): Promise<any> {
        const queueStatus = ["SUBMITTED","PENDING","RUNNABLE","STARTING","RUNNING","SUCCEEDED","FAILED"];
        let res = {};
        for (const jobStatus of queueStatus) {
            if (status) {
                if (status !== jobStatus) continue;
            }
            const definition = await this.batch.listJobs({
                jobQueue: queueName,
                jobStatus,
                maxResults: 100
            }).promise();
            res[jobStatus] = [];
            const {jobSummaryList} = definition;
            for(let summary of jobSummaryList) {
                const {jobId, startedAt, stoppedAt} = summary;
                res[jobStatus].push({
                    jobId,
                    startedAt,
                    stoppedAt
                });
            }
        }
        return res;
    }

    async getQueues(): Promise<any> {
        const qualityList: string[] = ['360', '480', '540', '720', '1080', 'sub', 'simple'];
        let jobQueueNameForm: string = null;
        if (this.envType === 'dev') {
            jobQueueNameForm = "publ-encoder-dev-job-queue-%s";
        } else {
            jobQueueNameForm = `publ-${this.envType}-encoder-job-queue-%s`;
        }

        let res = [];
        for (const stats of qualityList) {
           const queueName = util.format(jobQueueNameForm, stats);
           res.push(queueName);
        }
        return res;
    }

    async updateEndTime(data): Promise<number> {
        const {cid, liveId, startTime, endTime, bStatus, sStatus} = data;
        await this.liveRepo.createQueryBuilder()
            .update(LiveEntity)
            .set({
                endTime
            })
            .where("id= :id", { id: liveId})
            .execute();
        const liveData = await this.getLive(liveId);
        const buffer = Buffer.from(JSON.stringify({
            cid,
            id: liveId,
            broadcastStatus: bStatus,
            streamStatus: sStatus,
            streamUrl: `${this.streamUrl}/${liveId}/index.m3u8`,
            thumbnailUrl: liveData.thumbnailUrl,
            startTime,
            endTime
        }));

        const con = await amqp.connect(this.mqOption);
        const channel = await con.createChannel();
        await channel.prefetch(1);
        const queue = await channel.assertQueue(
            this.queue,
            {
                durable: true
            }
        );

        channel.sendToQueue(this.queue, buffer);
        return 200;
    }

    async updateStartTime(data): Promise<number> {
        const {cid, liveId, startTime, bStatus, sStatus} = data;
        await this.liveRepo.createQueryBuilder()
            .update(LiveEntity)
            .set({
                startTime
            })
            .where("id= :id", { id: liveId})
            .execute();
        const liveData = await this.getLive(liveId);
        const buffer = Buffer.from(JSON.stringify({
            cid,
            id: liveId,
            broadcastStatus: bStatus,
            streamStatus: sStatus,
            streamUrl: `${this.streamUrl}/${liveId}/index.m3u8`,
            thumbnailUrl: liveData.thumbnailUrl,
            startTime,
            endTime: new Date(new Date().setTime(new Date(startTime).getTime() + 2 * 60 * 60 * 1000)).toISOString()
        }));
        console.log({
            cid,
            id: liveId,
            broadcastStatus: bStatus,
            streamStatus: sStatus,
            streamUrl: `${this.streamUrl}/${liveId}/index.m3u8`,
            startTime,
            endTime: new Date(new Date().setTime(new Date(startTime).getTime() + 2 * 60 * 60 * 1000)).toISOString()
        });

        const con = await amqp.connect(this.mqOption);
        const channel = await con.createChannel();
        await channel.prefetch(1);
        const queue = await channel.assertQueue(
            this.queue,
            {
                durable: true
            }
        );

        channel.sendToQueue(this.queue, buffer);
        return 200;
    }

    private async getJobDescribe(jobIds: string[]): Promise<any> {
        let res: any[] = [];
        const jobDescribe: aws.Batch.Types.DescribeJobsResponse = await this.batch.describeJobs({
            jobs: jobIds
        }).promise();
        let {jobs} = jobDescribe;
        jobs.sort(function (jobA, jobB) {
            if (jobA.startedAt < jobB.startedAt) return 1;
            if (jobA.startedAt > jobB.startedAt) return -1;
            return 0;
        });
        for (let jobDetail of jobs) {
            const {container, jobId, startedAt} = jobDetail;
            const batchJobData = await this.vodBatchJobRepo.findOne({
                where: {
                    jobId
                }
            });
            if (batchJobData) {
                const {qId} = batchJobData;
                const qualityData = await this.vodQualityRepo.findOne({
                    where: {
                        id: qId
                    }
                });
                if (!qualityData) continue;
                const {quality} = qualityData;
                const {logStreamName} = container;
                const jobData = {
                    jobId,
                    logStreamName,
                    quality,
                    startedAt
                };
                res.push(jobData);
            }
        }

        return res;
    }

    async getLive(id): Promise<LiveEntity> {
        return await this.liveRepo.findOne({
            where : {
                id
            }
        });
    }
}
