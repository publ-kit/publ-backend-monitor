import {Body, Controller, Get, Headers, HttpException, HttpStatus, Param, Patch, Post, Query} from '@nestjs/common';
import { AppService } from './app.service';
import {GetConsumerIdQuery, GetVODTokenQuery, PagingModel, VODService, VODStatusRes} from "./vod.service";
import {LiveHistoryEntity, VODEntity} from "./db";
import {LiveService} from "./live.service";
import * as process from "process";
export class LiveHistoriesResModel extends LiveHistoryEntity {
    signedUrl: string;
}
export class GetRecordedVideoReqModel {
    streamKey: string;
}
export class LiveRecordReqModel {
    cid: string;
    lid: number;
}
export class RecordedVideo {
    videoUrl: string;
}
@Controller()
export class AppController {
    private readonly adminUsers: string[];
  constructor(
      private readonly appService: AppService,
      private readonly vodService: VODService,
      private readonly liveService: LiveService,
  ) {
      this.adminUsers = JSON.parse(process.env.ADMIN_USERS);
  }

  private customUserGuard(id: string) {
      if(!id) {
          throw new HttpException('UNAUTHORIZED', HttpStatus.UNAUTHORIZED);
      }
      if(!this.adminUsers.includes(id)) {
          throw new HttpException('FORBIDDEN', HttpStatus.FORBIDDEN);
      }
  }

  @Get('/batch')
  async getBatch(
      @Headers('x-monit-user-id') id,
      @Query() query: {
          status?: "SUBMITTED"|"PENDING"|"RUNNABLE"|"STARTING"|"RUNNING"|"SUCCEEDED"|"FAILED"|string,
          type?: "ENCODE"|"SUBTITLE"|"SIMPLE"
      }
  ): Promise<any[]> {
    const {status} = query;
    return this.appService.getEncodeJobList(status);
  }

  @Get('/logs')
  async logs(
      @Headers('x-monit-user-id') id,
      @Query() query: {
          logStreamName: string,
          nextToken?: string,
      }
  ): Promise<any> {
    console.log(query);
    return this.appService.getLogStream(query.logStreamName);
  }

  @Get('/login')
  async login(
      @Headers('x-monit-user-id') id,
    @Query() query: {host: string}
  ): Promise<any> {
    console.log(query);
    return this.appService.getLogin(query.host);
  }

  @Get('/auth')
  async auth(
      @Headers('x-monit-user-id') id,
     @Query() query: {host: string, code: string}
  ): Promise<any> {
     console.log(query);
     return this.appService.getAUTHUrl(query.host, query.code);
  }

  @Get('/logs/encoding')
  async encodingLogs(
      @Headers('x-monit-user-id') id,
     @Query() query: {uuid: string}
  ): Promise<any> {
     console.log(query);
     return this.appService.getEncodingsLog(query.uuid);
  }

  @Get("/logs/stream")
  async logsVODs(
      @Headers('x-monit-user-id') id,
      @Query() query: {
          startTime: Date,
          endTime: Date,
          metricName: string,
          target: string
      }
  ): Promise<any> {
      return this.appService.getVODLogStream(query);
  }

  @Post('/sync')
  async doSync(
      @Headers('x-monit-user-id') id,
      @Body() data: {
        cid: string,
        liveId: number,
        bStatus: string,
        sStatus: string
      }
  ): Promise<any> {
      this.customUserGuard(id);
    return this.appService.doSync(data);
  }

    @Post('/destroy')
    async doDestroy(
        @Headers('x-monit-user-id') id,
        @Body() data: {
            cid: string,
        }
    ): Promise<boolean> {
        this.customUserGuard(id);
      console.log(data);
      return this.appService.doDestroy(data.cid);
    }

    @Get('/batch/logs')
    async getBatchLogs(
        @Headers('x-monit-user-id') id,
        @Query() query: {
            jobId: string,
            nextBackwardToken?: string
        }
    ): Promise<any> {
      return this.appService.getBatchLogsByJobId(query.jobId, query.nextBackwardToken);
    }

    @Get('/batch/jobQueues')
    async getBatchJobQueues(
        @Headers('x-monit-user-id') id,
        @Query() query: {
            queueName: string,
            status: string
        }
    ): Promise<any> {
        return this.appService.getBatchJobQueuesByName(query.queueName, query.status);
    }

    @Get('/queues')
    async getQueues(): Promise<any> {
        return this.appService.getQueues();
    }

    @Patch('/live/end-time-update')
    async updateEndTime(
        @Headers('x-monit-user-id') id,
        @Body() body: any
    ): Promise<number> {
        this.customUserGuard(id);
        return this.appService.updateEndTime(body);
    }

    @Patch('/live/start-time-update')
    async updateStartTime(
        @Headers('x-monit-user-id') id,
        @Body() body: any
    ): Promise<number> {
        this.customUserGuard(id);
        return this.appService.updateStartTime(body);
    }

    @Get('/live-histories')
    async getLiveHistories(
        @Headers('x-monit-user-id') id,
        @Query() query: PagingModel
    ): Promise<LiveHistoriesResModel[]> {
        this.customUserGuard(id);
      return this.liveService.getLiveHistories(query);
    }

    @Get('/live-histories/record')
    async setLiveHistoriesRecord(
        @Headers('x-monit-user-id') id,
        @Query() query: LiveRecordReqModel
    ): Promise<boolean> {
        this.customUserGuard(id);
        return this.liveService.requestLiveHistoryRecord(query);
    }

    @Get('/live-histories/record/:lid')
    async getRecordedVideos(
        @Param("lid") lid: string,
        @Query() query: GetRecordedVideoReqModel
    ): Promise<RecordedVideo[]> {
        return this.liveService.getRecordedVideos(query.streamKey, lid);
    }

    @Get('/vods')
    async getVODs(
        @Query() query: PagingModel
    ): Promise<VODEntity[]> {
        return this.vodService.getVODs(query);
    }

    @Get('/vods/status')
    async getVODsStatus(
    ): Promise<VODEntity[]> {
        return this.vodService.getVODsStatus();
    }

    @Get('/vods/count')
    async getVODsCount(
    ): Promise<number> {
        return this.vodService.getVODsCount();
    }

    @Get('/vods/:vid')
    async getStatusByVODId(
        @Param('vid') vid: number
    ): Promise<VODStatusRes> {
      return this.vodService.getStatusByVODId(vid);
    }

    @Patch('/vods/:vid/repair')
    async repairVODById(
        @Headers('x-monit-user-id') id,
        @Param('vid') vid: number
    ): Promise<VODStatusRes> {
        this.customUserGuard(id);
      return this.vodService.repairVODById(vid);
    }

    @Patch('/vods/:vid/deduplication')
    async deduplicationVODById(
        @Headers('x-monit-user-id') id,
        @Param('vid') vid: number
    ): Promise<VODStatusRes> {
        this.customUserGuard(id);
        return this.vodService.deduplicationVODById(vid);
    }

    @Get('/vods/consumer-ids/:channelId')
    async getConsumerIds(
        @Param('channelId') channelId: string,
        @Query() query: GetConsumerIdQuery,
    ): Promise<string[]> {
        return this.vodService.getConsumerIds(channelId, query.vid);
    }

    @Get('/vods/:vid/token')
    async getVODPlayToken(
        @Param('vid') vid: number,
        @Query() query: GetVODTokenQuery,
    ): Promise<string> {
        return this.vodService.getVODToken(vid, query);
    }
}
