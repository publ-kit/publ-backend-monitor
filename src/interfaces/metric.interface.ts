
interface GetLiveMetricBody {
    consumerId: string;
}

interface LiveEncoderNetworkPacketModel {
    input: any;
    output: any;
}

interface LiveEncoderCpuModel {

}

interface GetLiveRelayMetricRes {
    name: string;
    network: LiveEncoderNetworkPacketModel;
}

interface GetLiveEncoderMetricRes {
    name: string;
    network: LiveEncoderNetworkPacketModel;
    cpu: any;
}

interface GetLiveProxyMetricRes {
    error4xx: any;
    error5xx: any;
    cpu: any;
}
