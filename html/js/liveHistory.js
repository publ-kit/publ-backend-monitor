const liveHistoryDataPage = document.getElementById('liveHistoryDataPage');
const liveHistoryData = document.getElementById('liveHistoryData');
const modal = document.getElementById("videoModal");
const modalContent = document.getElementById("videoModalContent");
// const recordedPlayer = document.getElementById("recordedPlayer");
const span = document.getElementsByClassName("close")[0];

const page = query?.page? query.page : 1;
liveHistoryDataPage.innerText = page.toString();
let time = 3;

async function searchLiveHistoryById() {
    const liveHistoryData = document.getElementById('liveHistoryData');
    liveHistoryData.innerText = "";
    const lidInput = document.getElementById('lidInput');
    await call('GET', `/live-histories?page=1&size=10&id=${lidInput.value}`, null, setLiveHistories);
}

async function liveHistoryDataChangePaging(num) {
    const page = Number(liveHistoryDataPage.innerText) + num;
    const liveHistoryData = document.getElementById('liveHistoryData');
    liveHistoryDataPage.innerText = page.toString();
    liveHistoryData.innerText = "";
    await call('GET', `/live-histories?page=${page}&size=10`, null, setLiveHistories);
}

async function call(method, url, data, callback) {
    await new Promise((res, rej) => {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        xhr.setRequestHeader('x-monit-user-id', user.id);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onreadystatechange = function () {
            let body = xhr.responseText;
            if (xhr.readyState === 4) {
                callback(body).then();
                res(xhr.responseText);
            }
        }
        xhr.send(data);
    });
}

setLiveHistories = async function(body) {
    const liveHistories = JSON.parse(body);
    const table = document.createElement('table');
    liveHistoryData.appendChild(table);
    table.id = 'liveHistoryTable';
    const thead = document.createElement('thead');
    const tbody = document.createElement('tbody');
    [
        thead,
        tbody
    ].map((node)=> {
        table.appendChild(node);
    });
    const theadTr = document.createElement('tr');
    const idTh = document.createElement('th');
    idTh.innerText = 'id';
    const cidTh = document.createElement('th');
    cidTh.innerText = 'consumerId';
    const lidTh = document.createElement('th');
    lidTh.innerText = 'liveId';
    const titleTh = document.createElement('th');
    titleTh.innerText = 'title';
    const totalViewerTh = document.createElement('th');
    totalViewerTh.innerText = 'totalViewer';
    const concurrentViewerTh = document.createElement('th');
    concurrentViewerTh.innerText = 'concurrentViewer';
    const startTimeTh = document.createElement('th');
    startTimeTh.innerText = 'startTime';
    const endTimeTh = document.createElement('th');
    endTimeTh.innerText = 'endTime';
    const recordStatusTh = document.createElement('th');
    recordStatusTh.innerText = 'recordStatus';
    const signedUrlTh = document.createElement('th');
    signedUrlTh.innerText = 'recorded live play';
    const requestTh = document.createElement('th');
    requestTh.innerText = 'recordRequest';
    [
        idTh,
        lidTh,
        cidTh,
        titleTh,
        totalViewerTh,
        concurrentViewerTh,
        startTimeTh,
        endTimeTh,
        recordStatusTh,
        signedUrlTh,
        requestTh
    ].map((node) => {
        theadTr.appendChild(node);
    });
    thead.appendChild(theadTr);
    for(const liveHistory of liveHistories) {
        const {id, cid, lid, title, totalViewer, concurrentViewer, startTime, endTime, recordStatus, streamKey, signedUrl} = liveHistory;
        const tbodyTr = document.createElement('tr');
        const idTd = document.createElement('td');
        idTd.innerText = id;
        const lidTd = document.createElement('td');
        lidTd.innerText = lid;
        const cidTd = document.createElement('td');
        cidTd.innerText = cid;
        const titleTd = document.createElement('td');
        titleTd.innerText = title;
        const totalViewerTd = document.createElement('td');
        totalViewerTd.innerText = totalViewer;
        const concurrentViewerTd = document.createElement('td');
        concurrentViewerTd.innerText = concurrentViewer;
        const startTimeTd = document.createElement('td');
        startTimeTd.innerText = new Date(startTime).toISOString();
        const endTimeTd = document.createElement('td');
        endTimeTd.innerText = new Date(endTime).toISOString();
        const recordStatusTd = document.createElement('td');
        recordStatusTd.innerText = recordStatus;
        recordStatusTd.style.backgroundColor = (recordStatus)? '#00ff00' : '#ff0000';
        const signedUrlTd = document.createElement('td');
        signedUrlTd.innerHTML = `<input type='button' onclick="recordedLivePlay('${streamKey}', '${lid}')" value="play">`;;
        const recordRequestTd = document.createElement('td');
        recordRequestTd.innerHTML = `<input type='button' onclick="recordRequest('${cid}', '${lid}')" value="request">`;
        [
            idTd,
            lidTd,
            cidTd,
            titleTd,
            totalViewerTd,
            concurrentViewerTd,
            startTimeTd,
            endTimeTd,
            recordStatusTd,
            signedUrlTd,
            recordRequestTd
        ].map((node)=>{
            tbodyTr.appendChild(node);
        });
        tbody.appendChild(tbodyTr);
    }

    console.log(liveHistories);
}

recordRequest = async function(cid, lid) {
    await call('GET', `/live-histories/record?cid=${cid}&lid=${lid}`, null, recordRequestRes);
}

recordRequestRes = async function(res) {
    console.log(res);
    if (res === "false") {
        alert('요청 할 수 없습니다!. (만들 수 있는 비디오가 없음)');
    } else {
        alert('요청 완료! 최대 10분 정도의 시간이 소요 됩니다.');
    }
}

recordedLivePlay = async function(streamKey, lid) {
    modal.style.display = "block";
    // <video class="videoPlayer" id="classNamededPlayer" controls></video>
    await call('GET', `/live-histories/record/${lid}?streamKey=${streamKey}`, null, setRecordVideos);
    // recordedPlayer.src = url;
}
let videos = [];
setRecordVideos = async function(res) {
    const videoId = "videoEl";
    let num = 1;
    for (const video of JSON.parse(res)) {
        const {videoUrl} = video;
        const videoEl = document.createElement('video');
        videoEl.src = videoUrl;
        videoEl.id = `${videoId}_${num}`;
        videoEl.width = 640;
        videoEl.height = 480;
        videoEl.controls = true;
        // videoEl.class = 'videoPlayer';
        modalContent.appendChild(videoEl);
        videos.push(videoEl.id);
        num +=1;
    }
}

span.onclick = function() {
    // recordedPlayer.src = "";
    for (const videoId of videos) {
        const videoEl = document.getElementById(videoId);
        videoEl.remove();
    }
    videos = [];
    modal.style.display = "none";
}

window.onclick = function(event) {
    if (event.target == modal) {
        // recordedPlayer.src = "";
        for (const videoId of videos) {
            const videoEl = document.getElementById(videoId);
            videoEl.remove();
        }
        videos = [];
        modal.style.display = "none";
    }
}

function getUrlParams() {
    let params = {};
    window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(str, key, value) { params[key] = value; });
    return params;
}

call('GET', `/live-histories?page=${page}&size=10`, null, setLiveHistories).then(()=>{});
