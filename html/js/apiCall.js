let liveData = {};
let setLive = false;
let setRedis = false;
let syncCheck = false;
const liveDataDiv = document.getElementById('liveData');
function getLives() {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', `/lives`, true);
    xhr.send();
    xhr.onreadystatechange = function() {
        let body = xhr.responseText;
        if (xhr.readyState === 4) {
            if (setLive) return null;
            setLive = true;
            let data = JSON.parse(body)["data"];
            console.log(data);
            for (let i in data) {
                const {cid, id, title, status, startTime, endTime, streamKey, metaData} = data[i];
                if (!liveData[cid]) {
                    liveData[cid] = {};
                }
                liveData[cid] = {
                    liveId: id,
                    startTime,
                    endTime,
                    broadcastStatus: status,
                    streamKey,
                    metaData,
                    title
                };
            }
        }

        getRedis();
    }
}

function getRedis() {
    liveDataDiv.innerText = "";
    const liveMonitTable = document.createElement('table');
    const liveMonitTh = document.createElement('thead');
    const liveMonitThTr = document.createElement('tr');
    const liveMonitHeaders = ['Status', 'Title', 'Channel Code', 'Consumer Id', 'Live Id', 'Live Status', 'Chat Status', 'Start Time', 'End Time'];
    for (const lmh of liveMonitHeaders) {
        const header = document.createElement('th');
        header.innerText = lmh;
        liveMonitThTr.append(header);
    }
    liveMonitTh.append(liveMonitThTr);
    const liveMonitTb = document.createElement('tbody');
    liveMonitTb.id = 'liveDataTbody';
    liveMonitTable.append(liveMonitTh);
    liveMonitTable.append(liveMonitTb);
    liveDataDiv.append(liveMonitTable);
    const xhr = new XMLHttpRequest();
    xhr.open('GET', '/redis-status', true);
    xhr.send();
    xhr.onreadystatechange = function() {
        let body = xhr.responseText;
        if (xhr.readyState === 4) {
            if (setRedis) return null;
            setRedis = true;
            let data = JSON.parse(body)["data"];
            for (let cid in data) {
                if (liveData[cid]) {
                    console.log(cid);
                    console.log("=========server status========");
                    console.log(liveData[cid]);
                    console.log("=========signal status========");
                    console.log(data[cid]);
                    if (liveData[cid].broadcastStatus !== data[cid].broadcastStatus) {
                        appendLiveData(cid, liveData[cid].liveId, liveData[cid].broadcastStatus, data[cid].broadcastStatus, 'RED', data[cid].streamStatus);
                    } else {
                        appendLiveData(cid, liveData[cid].liveId, liveData[cid].broadcastStatus, data[cid].broadcastStatus, 'GREEN', data[cid].streamStatus);
                    }
                } else {
                    liveData[cid] = data[cid];
                    let status = 'RED';
                    if (liveData[cid].broadcastStatus === 'NONE' ){
                        status = 'GREEN';
                    }
                    appendLiveData(cid, liveData[cid].liveId, null, liveData[cid].broadcastStatus, status, data[cid].streamStatus);
                }
            }
            console.log(liveData);
        }
    }
}

function makeChannelCode(codeString) {
    const base64String = btoa(codeString);
    return base64String
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/\=+$/, '');
}

function appendLiveData(cid, id, liveStatus, redisStatus, syncStatus, streamStatus) {
    if (liveStatus == null && redisStatus == 'NONE') {
        return null;
    }

    const setLabel = document.createElement('label');
    setLabel.innerHTML = syncStatus;
    setLabel.style.color = syncStatus;
    setLabel.for = cid;
    const setDiv = document.createElement('div');
    setDiv.id = cid;
    setDiv.name = cid;
    setDiv.style.display = 'inline-table';
    setDiv.style.margin = 19;
    const [publ,channelId,pAppCode,pAppId] = cid.split("-");
    const channelCode = makeChannelCode(`/channels/${channelId}`);
    console.log(liveData[cid]);
    const title = liveData[cid]?.title? liveData[cid].title : "live data not found";
    const endTime = liveData[cid]?.endTime? new Date(liveData[cid].endTime).toISOString() : "live data not found";

    // tr td setting
    const ldtDataTr = document.createElement('tr');
    const ldtStatus = document.createElement('td');
    const ldtTitle = document.createElement('td');
    const ldtChannelCode = document.createElement('td');
    const ldtConsumerId = document.createElement('td');
    const ldtLiveId = document.createElement('td');
    const ldtLiveStatus = document.createElement('td');
    const ldtChatStatus = document.createElement('td');
    const ldtStartTime = document.createElement('td');
    const ldtEndTime = document.createElement('td');
    const ldtBtnTr = document.createElement('tr');
    const ldtBtnTd = document.createElement('td');
    ldtBtnTd.colSpan = 9;

    // td inner data
    ldtStatus.append(setLabel);
    ldtTitle.innerText = title;
    ldtChannelCode.innerText = channelCode;
    ldtConsumerId.innerText = cid;
    ldtLiveId.innerText = id;
    ldtLiveStatus.innerHTML = `<text style='color:RED'>${liveStatus}</text>`;
    ldtChatStatus.innerHTML = `<text style='color:BLUE'>${redisStatus}</text>`;
    ldtStartTime.innerText = liveData[cid].startTime;
    ldtEndTime.innerText = endTime;

    // tr append td
    ldtDataTr.append(ldtStatus);
    ldtDataTr.append(ldtTitle);
    ldtDataTr.append(ldtChannelCode);
    ldtDataTr.append(ldtConsumerId);
    ldtDataTr.append(ldtLiveId);
    ldtDataTr.append(ldtLiveStatus);
    ldtDataTr.append(ldtChatStatus);
    ldtDataTr.append(ldtStartTime);
    ldtDataTr.append(ldtEndTime);

    // tbody append tr
    const liveMonitTb = document.getElementById('liveDataTbody');
    liveMonitTb.append(ldtDataTr);
    setDiv.innerHTML = 'title: ['+title+'] | channel code : '+channelCode+' | cid :  ' + cid + '  |  live id  : ' + id + '  |  db Status :  ' + liveStatus + '  |  redis Status  :  ' + redisStatus + ' | endTime : ' + endTime;

    const setBtn = document.createElement('button');
    setBtn.addEventListener("click", async function () {
        if (confirm('[WARNING] This behavior may affect live performance. Do you want to proceed?')) {
            if (syncCheck) {
                alert('Already sync processing!');
                return null;
            }
            let endStatus = liveStatus;
            if (redisStatus === 'ON_AIR') {
                if (!liveStatus) {
                    endStatus = 'TERMINATED';
                }
            }
            await doSync(cid, redisStatus, endStatus, id, streamStatus);
        }
    });
    setBtn.innerHTML = 'do sync';

    const destroyBtn = document.createElement('button');
    destroyBtn.addEventListener("click", async function () {
        if (syncCheck) {
            alert('Already sync processing!');
            return null;
        }
        const destroy = prompt('Type \'yes\' if you want destroy live streaming ', '');
        if (destroy === 'yes') {
            syncCheck=true;
            await callDestroy(JSON.stringify({cid}));
        }
    });
    destroyBtn.innerHTML = 'destroy';
    destroyBtn.style.backgroundColor = 'RED';
    destroyBtn.style.color = 'white';

    const backupBtn = document.createElement('button');
    backupBtn.addEventListener("click", async function () {
        if (syncCheck) {
            alert('Already sync processing!');
            return null;
        }
        const backupCid = prompt('Type backup cid ', '');
        if (backupCid) {
            if (liveData[backupCid]) {
                syncStatus=true;
                await callSync(JSON.stringify({
                    cid,
                    liveId: `${liveData[cid].liveId}`,
                    bStatus: liveStatus,
                    sStatus: "OUT_STREAM",
                    startTime: liveData[cid].startTime
                }));
                await sleep(1000);
                await backup(
                    liveData[cid].liveId,
                    liveData[backupCid].liveId,
                    liveData[backupCid].streamKey,
                    liveData[cid].streamKey,
                    liveData[backupCid].metaData,
                    cid
                );
                await sleep(1000);
                await callSync(JSON.stringify({
                    cid,
                    liveId: `${liveData[cid].liveId}`,
                    bStatus: liveStatus,
                    sStatus: "ON_STREAM",
                    startTime: liveData[cid].startTime
                }));
                syncCheck=false;
            }
        }
    });
    backupBtn.innerHTML = 'backup';
    backupBtn.style.backgroundColor = 'RED';
    backupBtn.style.color = 'white';

    // play btn
    const playBtn = document.createElement('button');
    playBtn.addEventListener("click", async function () {
        const url = `/live/${liveData[cid].streamKey}/${liveData[cid].liveId}/playlist.m3u8`;
        window.open(url);
    });
    playBtn.innerHTML = 'play';
    playBtn.style.backgroundColor = 'GREEN';
    playBtn.style.color = 'white';

    // on stream
    const onBtn = document.createElement('button');
    onBtn.addEventListener("click", async function () {
        if (confirm("Do you want to process ON_STREAM for this live?")) {
            await callSync(JSON.stringify({
                cid,
                liveId: `${liveData[cid].liveId}`,
                bStatus: liveStatus,
                sStatus: "ON_STREAM",
                startTime: liveData[cid].startTime
            }));
        }
    });
    onBtn.innerHTML = 'on';
    onBtn.style.backgroundColor = 'GREEN';
    onBtn.style.color = 'white';

    // update end time
    const updateEndTimeBtn = document.createElement('button');
    updateEndTimeBtn.addEventListener("click", async function () {
        const endTimePt = prompt('Enter endTime', `${new Date(new Date().setTime(new Date().getTime() + 2 * 60 * 60 * 1000)).toISOString()}`);
        console.log(endTimePt);
        const validDate = validTime(endTimePt, liveData[cid].startTime);
        if (validDate) {
            await callUpdate(JSON.stringify({
                cid,
                liveId: `${liveData[cid].liveId}`,
                bStatus: liveStatus,
                sStatus: streamStatus,
                startTime: liveData[cid].startTime,
                endTime: endTimePt
            }), 'end-time-update');
        }
    });
    updateEndTimeBtn.innerHTML = 'update endTime';
    updateEndTimeBtn.style.backgroundColor = 'RED';
    updateEndTimeBtn.style.color = 'white';

    // update start time
    const updateStartTimeBtn = document.createElement('button');
    updateStartTimeBtn.addEventListener("click", async function () {
        const startTimePt = prompt('Enter StartTime', `${new Date().toISOString()}`);
        console.log(startTimePt);
        if (liveStatus !== 'STAND_BY') {
            alert('[ERROR] Changing the live start time is only possible when live is in STAND_BY state!!')
        } else {
            await callUpdate(JSON.stringify({
                cid,
                liveId: `${liveData[cid].liveId}`,
                bStatus: liveStatus,
                sStatus: streamStatus,
                // startTime: liveData[cid].startTime,
                startTime: startTimePt
            }), 'start-time-update');
        }
    });
    updateStartTimeBtn.innerHTML = 'update startTime';
    updateStartTimeBtn.style.backgroundColor = 'RED';
    updateStartTimeBtn.style.color = 'white';


    // liveDataDiv.append(setLabel);
    // liveDataDiv.append(setDiv);
    // liveDataDiv.append(setBtn);
    // liveDataDiv.append(destroyBtn);
    // liveDataDiv.append(backupBtn);
    // liveDataDiv.append(playBtn);
    // liveDataDiv.append(onBtn);
    // liveDataDiv.append(updateEndTimeBtn);
    // liveDataDiv.append(updateStartTimeBtn);

    // ldtBtnTd.append(setLabel);
    // ldtBtnTd.append(setDiv);
    ldtBtnTd.append(setBtn);
    ldtBtnTd.append(destroyBtn);
    ldtBtnTd.append(backupBtn);
    ldtBtnTd.append(playBtn);
    ldtBtnTd.append(onBtn);
    ldtBtnTd.append(updateEndTimeBtn);
    ldtBtnTd.append(updateStartTimeBtn);
    if (liveStatus === 'STAND_BY' || liveStatus === 'ON_AIR') {
        ldtBtnTd.id = id;
        callEncoderStatus(id);
        // setDiv.append(encodeData);
        // ldtBtnTd.append(encodeData);
    }

    ldtBtnTr.append(ldtBtnTd);
    liveMonitTb.append(ldtBtnTr);

    const setBR = document.createElement('br');
    // liveDataDiv.append(setBR);
}

async function backup(
    originLiveId,
    targetLiveId,
    targetStreamKey,
    originStreamKey,
    targetMetaData,
    originCid
) {
    const data = {
        originLiveId,
        targetLiveId,
        targetStreamKey,
        originStreamKey,
        targetMetaData,
        originCid
    };
    await new Promise((res, rej) => {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '/backup', true);
        xhr.setRequestHeader('x-monit-user-id', user.id);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onreadystatechange = function () {
            let body = xhr.responseText;
            if (xhr.readyState === 4) {
                console.log(body);
                res(xhr.responseText);
            }
        }
        xhr.send(JSON.stringify(data));
    });
}

function callEncoderStatus(id) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', '/encoder-status?id=' + id, true);
    xhr.setRequestHeader('x-monit-user-id', user.id);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.onreadystatechange = function () {
        let body = xhr.responseText;
        if (xhr.readyState === 4) {
            console.log(body);
            const jsonData = JSON.parse(body);
            let logData = {};
            for (let quality in jsonData) {
                console.log(`quality : ${quality}`);
                logData[quality] = {};
                let tmpLogData = {};
                const logStr = jsonData[quality];
                const logStrParse = logStr.split("    ");
                const lastLog = logStrParse[logStrParse.length - 2];
                const logParse = lastLog.split(" ");
                let j = 0;
                for (let i = 0; i < logParse.length; i++) {
                    let key = logParse[i];
                    if (key.includes('frame')) {
                        let keyParse = key.split("=");
                        tmpLogData.frame = keyParse[keyParse.length - 1];
                    } else if (key.includes("fps")) {
                        tmpLogData.fps = logParse[i + 1];
                    } else if (key.includes("speed")) {
                        tmpLogData.speed = logParse[i + 3];
                    } else {
                        if (key.includes("=")) {
                            let keyParse = key.split("=");
                            tmpLogData[keyParse[0]] = keyParse[1];
                        }
                    }
                }
                console.log(tmpLogData);
                logData[quality] = tmpLogData;
            }

            // const liveDiv = document.getElementById(id);
            // liveDiv.innerHTML = "";
            const encoderStatusDiv = document.getElementById(id);
            for (let quality in logData) {
                const button = document.createElement('text');
                button.innerText = quality;
                button.style.border = '1px solid BLACK';
                // const title = document.createElement('h3');
                // title.innerText = quality;
                const text = document.createElement('p');
                text.id = quality;
                text.className = 'content';
                text.innerText = JSON.stringify(logData[quality]);
                button.addEventListener('click', function () {
                    const content = document.getElementById(quality);
                    if (content.style.display === 'block') {
                        content.style.display = 'none';
                    } else {
                        content.style.display = 'block';
                    }
                })
                // text.style.width = "910px";
                // liveDiv.append(title);
                encoderStatusDiv.append(button);
                encoderStatusDiv.append(text);
                // liveDiv.append(text);
            }
        }
    }
    xhr.send();
}

async function callSync(data) {
    await new Promise((res, rej) => {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '/sync', true);
        xhr.setRequestHeader('x-monit-user-id', user.id);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onreadystatechange = function () {
            let body = xhr.responseText;
            if (xhr.readyState === 4) {
                console.log(body);
                res(xhr.responseText);
            }
        }
        xhr.send(data);
    });
}

async function callUpdate(data, endPoint) {
    await new Promise((res, rej) => {
        let xhr = new XMLHttpRequest();
        xhr.open('PATCH', `/live/${endPoint}`, true);
        xhr.setRequestHeader('x-monit-user-id', user.id);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onreadystatechange = function () {
            let body = xhr.responseText;
            if (xhr.readyState === 4) {
                console.log(body);
                res(xhr.responseText);
                location.reload();
            }
        }
        xhr.send(data);
    });
}

async function callToken(data, url) {
    await new Promise((res, rej) => {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '/token', true);
        xhr.setRequestHeader('x-monit-user-id', user.id);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onreadystatechange = function () {
            let body = xhr.responseText;
            if (xhr.readyState === 4) {
                console.log(body);
                res(xhr.responseText);
                location.replace(url)
            }
        }
        xhr.send(data);
    });
}

async function callDestroy(data) {
    await new Promise((res, rej) => {
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '/destroy', true);
        xhr.setRequestHeader('x-monit-user-id', user.id);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onreadystatechange = function () {
            let body = xhr.responseText;
            if (xhr.readyState === 4) {
                console.log(body);
                res(xhr.responseText);
                syncCheck=false;
            }
        }
        xhr.send(data);
    });
}

async function doSync(cid, startStatus, endStatus, liveId, streamStatus) {
    syncCheck=true;
    console.log(cid);

    let callStatus = [];

    console.log(endStatus);

    if (endStatus === 'INITIALIZING') {
        callStatus = ['PUBLISHED', 'INITIALIZING'];
    }
    if (endStatus === 'STAND_BY') {
        callStatus = ['PUBLISHED', 'INITIALIZING', 'STAND_BY'];
    }
    if (endStatus === 'ON_AIR') {
        callStatus = ['PUBLISHED', 'INITIALIZING', 'STAND_BY', 'ON_AIR'];
    }
    if (endStatus === 'CANCELED') {
        callStatus = ['PUBLISHED', 'INITIALIZING', 'STAND_BY', 'CANCELED'];
    }
    if (endStatus === 'TERMINATED') {
        callStatus = ['PUBLISHED', 'INITIALIZING', 'STAND_BY', 'ON_AIR', 'TERMINATED'];
    }
    if (!endStatus) {
        callStatus = ['PUBLISHED', 'INITIALIZING', 'STAND_BY', 'CANCELED'];
    }
    console.log(callStatus);
    const a = document.getElementById('liveData');
    let syncStr = "Sync processing!";
    a.innerHTML = `<h1> ${syncStr} </h1>`;
    for (let broadcastStatus of callStatus) {
        let data = {
            cid,
            liveId: `${liveId}`,
            bStatus: broadcastStatus,
            sStatus: streamStatus
        };
        let callData = JSON.stringify(data);
        syncStr += "..";
        a.innerHTML = `<h1> ${syncStr} </h1>`;
        await callSync(callData);
        await sleep(2000);
    }
    syncCheck = false;
    location.href = "";
}

function setCallNum(addNum, endStatus) {
    let callNum = -1;
    switch(endStatus) {
        case 'PUBLISHED' :
            callNum = addNum+0;
            break;
        case 'INITIALIZING' :
            callNum = addNum+1;
            break;
        case 'STAND_BY' :
            callNum = addNum+2;
            break;
        case 'ON_AIR':
            callNum = addNum+3;
            break;
        case 'CANCELED':
            callNum = addNum+2;
            break;
        default:
            callNum = null;
            break;
    }
    return callNum;
}

function setCall(inum, num, endNum) {
    let callStatus = [];

    if (endNum === 0) {
        console.log("!");
        if (inum === 4) {
            callStatus.push('TERMINATED');
        } else if (inum === 0){
            callStatus = [];
        } else {
            callStatus.push('CANCELED');
        }
    } else {
        let statusList = ['PUBLISHED', 'INITIALIZING', 'STAND_BY', 'ON_AIR'];
        for (let i = inum; i < num; i++ ){
            callStatus.push(statusList[i]);
        }
    }
    return callStatus;
}

let clickedStatus = null;

// function getBatchList(ele) {
//     if (clickedStatus) {
//         clickedStatus.style.border = "3px solid black";
//     }
//     clickedStatus = ele;
//     ele.style.border = "3px solid red";
//     const status = ele.innerText;
//     const batchList = document.getElementById('batchList');
//     batchList.innerHTML = "";
//     let xhr = new XMLHttpRequest();
//     xhr.open('GET', `/batch?status=${status}`, true);
//     xhr.setRequestHeader('Content-type', 'application/json');
//     xhr.onreadystatechange = function () {
//         if (xhr.readyState === 4) {
//             let body = xhr.responseText;
//             const data = JSON.parse(body);
//             if (data.length > 0) {
//                 for (let job of data) {
//                     const {jobId, quality, logStreamName, startedAt} = job;
//                     const p = document.createElement('p');
//                     p.innerText = `quality: ${quality} | jobId : ${jobId} | startedAt: ${new Date(startedAt).toISOString()}`;
//                     p.addEventListener('click', function () {
//                         getBatchLog(logStreamName);
//                     });
//                     batchList.append(p);
//                 }
//             } else {
//                 const p = document.createElement('p');
//                 p.innerText = 'None';
//                 batchList.append(p);
//             }
//         }
//     }
//     xhr.send();
// }

async function getBatchList(status) {
    if (clickedStatus) {
        clickedStatus.style.border = "3px solid black";
    }
    clickedStatus = status;
    status.style.border = "3px solid red";
    const statusName = status.innerText;
    const batchList = document.getElementById('batchList');
    batchList.innerHTML = "";
    const queueRadio = document.querySelector('select[name="queueName"] option:checked');
    if (!queueRadio.value) {
        return null;
    }
    const queueName = queueRadio.value;

    const table = document.createElement('table');
    const thead = document.createElement('thead');
    const headTr = document.createElement('tr');
    const jobIdTh = document.createElement('th');
    jobIdTh.innerHTML = 'jobId';
    const startTh = document.createElement('th');
    startTh.innerHTML = 'startedAt';
    const stopTh = document.createElement('th');
    stopTh.innerHTML = 'stoppedAt';
    const buttonTh = document.createElement('th');
    buttonTh.innerHTML = 'get log';
    headTr.appendChild(jobIdTh);
    headTr.appendChild(startTh);
    headTr.appendChild(stopTh);
    headTr.appendChild(buttonTh);
    thead.appendChild(headTr);
    table.appendChild(thead);
    const tbody = document.createElement('tbody');
    table.appendChild(tbody);
    batchList.appendChild(table);
    await new Promise((res, rej) => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', `/batch/jobQueues?queueName=${queueName}&status=${statusName}`, true);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onreadystatechange = function () {
            let body = xhr.responseText;
            if (xhr.readyState === 4) {
                console.log(body);
                const jobQueues = JSON.parse(body);
                const jobs = jobQueues[statusName];
                for (const job of jobs) {
                    const {jobId, startedAt, stoppedAt} = job;
                    const bodyTr = document.createElement('tr');
                    const jobIdTd = document.createElement('td');
                    jobIdTd.innerHTML = jobId;
                    const startTd = document.createElement('td');
                    if (startedAt) {
                        startTd.innerHTML = new Date(startedAt).toISOString();
                    } else {
                        startTd.innerHTML = "-";
                    }
                    const stopTd = document.createElement('td');
                    if (stoppedAt) {
                        stopTd.innerHTML = new Date(stoppedAt).toISOString();
                    } else {
                        stopTd.innerHTML = '-';
                    }
                    const button = document.createElement('input');
                    button.type = 'button';
                    button.value = 'Get Log';
                    button.addEventListener('click', function () {
                        getBatchLogByJobId(jobId);
                    });
                    bodyTr.appendChild(jobIdTd);
                    bodyTr.appendChild(startTd);
                    bodyTr.appendChild(stopTd);
                    bodyTr.appendChild(button);
                    tbody.appendChild(bodyTr);
                }
                res(body);
            }
        }
        xhr.send();
    });
}

function backToBatchList() {
    const logs = document.getElementById('logs');
    logs.innerHTML = '';
    logs.style.display = 'none';
    const batchList = document.getElementById('batchList');
    batchList.style.display = 'block';
}
function dotLoading(){
    const modal = document.getElementById('loadingText');
    let dotStr = "";
    if (loadingDot < 5) {
        loadingDot+=1;
    } else {
        loadingDot = 1;
    }
    for (let i=0;i<=loadingDot;i++) {
        dotStr+=".";
    }
    modal.innerText = "loading"+dotStr;
}
let loadingDot = 0;
async function callBatchLogs(logs, modal, loading, jobId, token) {
    await new Promise((res, rej) => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', `/batch/logs?jobId=${jobId}&nextBackwardToken=${token}`, true);
        xhr.setRequestHeader('x-monit-user-id', user.id);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onreadystatechange = function () {
            let body = xhr.responseText;
            if (xhr.readyState === 4) {
                console.log(body);
                const {messages, nextBackwardToken} = JSON.parse(body);
                if (messages.length > 0) {
                    const p = document.createElement('textarea');
                    let str = ""
                    for (const message of messages) {
                        str += message + "\n";
                    }
                    p.value = str;
                    p.style.width = "100%";
                    const button = document.createElement('input');
                    button.type = 'button';
                    button.value = 'back to list';
                    button.addEventListener('click', function () {
                        backToBatchList();
                    });
                    logs.appendChild(p);
                    logs.appendChild(button);

                    modal.style.display = "none";
                    clearInterval(loading);
                    const obj = document.querySelector('textarea');
                    console.log(`[${new Date().toISOString()}] ${obj.scrollHeight}`);
                    obj.style.height = (12+obj.scrollHeight)+"px";
                    console.log(`[${new Date().toISOString()}] done`);
                } else {
                    callBatchLogs(logs, modal, loading, jobId, nextBackwardToken);
                }
            }
            res(body);
        }
        xhr.send();
    });

}

async function getBatchLogByJobId(jobId) {
    const logs = document.getElementById('logs');
    logs.style.display = 'block';
    const batchList = document.getElementById('batchList');
    batchList.style.display = 'none';
    const modal = document.getElementById('loadingModal');
    modal.style.display = 'block';
    let loading = setInterval(dotLoading,1000);
    console.log(`[${new Date().toISOString()}] start`);
    await new Promise((res, rej) => {
        let body = callBatchLogs(logs, modal, loading, jobId, null);
        res(body);
    });
}

function getBatchLog(logStreamName) {
    console.log(logStreamName);
    const batchList = document.getElementById('batchList');
    batchList.innerHTML = "";
    let xhr = new XMLHttpRequest();
    xhr.open('GET', `/logs?logStreamName=${logStreamName}`, true);
    xhr.setRequestHeader('x-monit-user-id', user.id);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            let body = xhr.responseText;
            const data = JSON.parse(body);
            const {messages} = data;
            const btn = document.createElement('p');
            btn.addEventListener('click', function () {
                getBatchLog(logStreamName);
            });
            btn.innerText = 'reload';
            btn.style.border = '3px solid black';
            batchList.append(btn);
            for (let message of messages) {
                const p = document.createElement('p');
                p.innerText = message;
                batchList.append(p);
            }
        }
    }
    xhr.send();
}

async function sleep(ms) {
    return new Promise(res => {
        setTimeout(res, ms);
    })
}

async function getEncodingLog() {
    const logData = document.getElementById('logData');
    const getLogInput = document.getElementById('getLogInput');
    const logVidText = document.getElementById('logVidText');
    const uuid = getLogInput.value;
    logData.innerText = '';
    await new Promise((res, rej) => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', `/logs/encoding?uuid=${uuid}`, true);
        xhr.setRequestHeader('x-monit-user-id', user.id);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onreadystatechange = function () {
            let body = xhr.responseText;
            if (xhr.readyState === 4) {
                console.log(body);
                const {vid, qualities} = JSON.parse(body);
                logVidText.innerText = vid;
                for (const qualityData of qualities) {
                    const {quality, jobLogStreams} = qualityData;
                    const logDataText = document.createElement('textarea');
                    let logStr = "";
                    for (const logStream of jobLogStreams) {
                        logStr = logStr + '\r\n' + logStream;
                    }
                    logDataText.style = "width:100%;height:10%";
                    logDataText.value = logStr;
                    logData.append(logDataText);
                }
                res(body);
            }
        }
        xhr.send();
    });
}

async function getQueueList() {
    const queueList = document.getElementById('queueList');
    const selector = document.createElement('select');
    selector.name = "queueName";
    await new Promise((res, rej) => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', `/queues`, true);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.setRequestHeader('x-monit-user-id', user.id);
        xhr.onreadystatechange = function () {
            let body = xhr.responseText;
            if (xhr.readyState === 4) {
                console.log(body);
                const queues = JSON.parse(body);
                for (const queue of queues) {
                    const option = document.createElement('option');
                    option.type = 'radio';
                    option.value = queue;
                    option.innerText += queue;
                    selector.appendChild(option);
                }
                queueList.appendChild(selector);
                res(body);
            }
        }
        xhr.send();
    });
}

function validTime(endTime, startTime) {
    try {
        new Date(endTime);
    } catch (e) {
        console.error(`Invalid time format : ${endTime}`);
        return false;
    }
    const checkEnd = new Date(endTime).getTime();
    const checkStart = new Date(startTime).getTime();
    return checkStart < checkEnd;
}

getQueueList().then(() => null);
