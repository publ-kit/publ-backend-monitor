let intervalTime = 60;
let paused = false;
let timer;
function re() {
    if (intervalTime === 0 ){
        location.reload();
    }
    const id = document.getElementById("refreshTimer");
    id.innerHTML = `${intervalTime--}s`;
}
function pause() {
    const pauseBtn = document.getElementById('pauseBtn');
    if (paused) {
        pauseBtn.innerHTML = '||';
        timer = setInterval(re, 1000);
        paused = false;
    } else {
        pauseBtn.innerHTML = '>'
        clearInterval(timer);
        paused = true;
    }
}
timer = setInterval(re, 1000);
