

const liveMetricData = document.getElementById('liveMetricData');
const liveCommonMetricData = document.getElementById('liveCommonMetricData');

function makeChart(labels, datasets, type="line", element) {
    const data = {
        labels,
        datasets
    };

    new Chart(element, {
        type,
        data,
        options: {}
    });
}

function getChartLabel(metricResults) {
   return metricResults[0].Timestamps.reverse().map((res) => {
       const time = new Date(res);
       return `${time.getHours()}:${time.getMinutes()}`;
   });
}

async function getLiveEncoderMetric(consumerId) {
    new Promise((res, rej) => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', `/metric/live/encoder?consumerId=${consumerId}`, true);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onreadystatechange = function () {
            let body = xhr.responseText;
            if (xhr.readyState === 4) {
                const data = JSON.parse(body);
                const encoderCPUMetricChart = document.createElement('canvas');
                encoderCPUMetricChart.id = `encoder-cpu-metric-${consumerId}`;
                const encoderNetworkInMetricChart = document.createElement('canvas');
                encoderNetworkInMetricChart.id = `encoder-network-in-metric-${consumerId}`;
                const encoderNetworkOutMetricChart = document.createElement('canvas');
                encoderNetworkOutMetricChart.id = `encoder-network-out-metric-${consumerId}`;
                let labels1 = null;
                let labels2 = null;
                let labels3 = null;
                let datasets1 = [];
                let datasets2 = [];
                let datasets3 = [];
                const rgbList = [
                    'rgb(255, 0, 0)',
                    'rgb(0, 255, 0)',
                    'rgb(0, 0, 255)',
                    'rgb(255, 255, 0)'
                ];
                let rgbCheck = 0;
                for (const tmpData of data) {
                    const {name, network, cpu} = tmpData;
                    if (labels1 == null) {
                        const labels1Time = network.input.MetricDataResults[0].Timestamps.reverse().map((res) => {
                            const time = new Date(res);
                            return `${time.getHours()}:${time.getMinutes()}`;
                        });
                        labels1 = labels1Time;
                    }
                    if (labels2 == null) {
                        const labels2Time = network.output.MetricDataResults[0].Timestamps.reverse().map((res) => {
                            const time = new Date(res);
                            return `${time.getHours()}:${time.getMinutes()}`;
                        });
                        labels2 = labels2Time;
                    }
                    if (labels3 == null) {
                        const labels3Time = cpu.MetricDataResults[0].Timestamps.reverse().map((res) => {
                            const time = new Date(res);
                            return `${time.getHours()}:${time.getMinutes()}`;
                        });
                        labels3 = labels3Time;
                    }
                    const splitName = name.split("-");

                    datasets1.push({
                        label: `network-in-${splitName[splitName.length-1]}`,
                        backgroundColor: rgbList[rgbCheck],
                        borderColor: rgbList[rgbCheck],
                        data: network.input.MetricDataResults[0].Values.reverse()
                    });
                    datasets2.push({
                        label: `network-out-${splitName[splitName.length-1]}`,
                        backgroundColor: rgbList[rgbCheck],
                        borderColor: rgbList[rgbCheck],
                        data: network.output.MetricDataResults[0].Values.reverse()
                    });
                    datasets3.push({
                        label: `cpu-${splitName[splitName.length-1]}`,
                        backgroundColor: rgbList[rgbCheck],
                        borderColor: rgbList[rgbCheck],
                        data: cpu.MetricDataResults[0].Values.reverse()
                    });
                    rgbCheck += 1;
                }

                const networkChartData = {
                    labels: labels1,
                    datasets: datasets1
                };
                const network2ChartData = {
                    labels: labels2,
                    datasets: datasets2
                };
                const cpuChartData = {
                    labels: labels3,
                    datasets: datasets3
                };


                new Chart(encoderNetworkInMetricChart, {
                    type: 'line',
                    data: networkChartData,
                    options: {}
                });
                new Chart(encoderNetworkOutMetricChart, {
                    type: 'line',
                    data: network2ChartData,
                    options: {}
                });
                new Chart(encoderCPUMetricChart, {
                    type: 'line',
                    data: cpuChartData,
                    options: {}
                });
                const network1Div = document.createElement('div');
                const network2Div = document.createElement('div');
                const cpuDiv = document.createElement('div');
                network1Div.style.width = "30%";
                network2Div.style.width = "30%";
                cpuDiv.style.width = "30%";
                network1Div.append(encoderNetworkInMetricChart);
                network2Div.append(encoderNetworkOutMetricChart);
                cpuDiv.append(encoderCPUMetricChart);
                liveMetricData.append(network1Div);
                liveMetricData.append(network2Div);
                liveMetricData.append(cpuDiv);
                res(body);
            }
        }
        xhr.send();
    });
}

async function getLiveRelayMetric(consumerId) {
    new Promise((res, rej) => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', `/metric/live/relay?consumerId=${consumerId}`, true);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onreadystatechange = function () {
            let body = xhr.responseText;
            if (xhr.readyState === 4) {
                console.log(body);
                const data = JSON.parse(body);
                let datasets1 = [];
                let datasets2 = [];
                const rgbList = [
                    'rgb(255, 0, 0)',
                    'rgb(0, 255, 0)',
                    'rgb(0, 0, 255)',
                    'rgb(255, 255, 0)'
                ];
                let rgbCheck = 0;
                const relayNetworkInMetricChart = document.createElement('canvas');
                relayNetworkInMetricChart.id = `relay-network-in-metric-${consumerId}`;
                const relayNetworkOutMetricChart = document.createElement('canvas');
                relayNetworkOutMetricChart.id = `relay-network-out-metric-${consumerId}`;
                const {name, network} = data;

                const labels1 = getChartLabel(network.input.MetricDataResults);
                const labels2 = getChartLabel(network.output.MetricDataResults);
                datasets1.push({
                    label: `network-in-relay`,
                    backgroundColor: rgbList[rgbCheck],
                    borderColor: rgbList[rgbCheck],
                    data: network.input.MetricDataResults[0].Values.reverse()
                });
                datasets2.push({
                    label: `network-out-relay`,
                    backgroundColor: rgbList[rgbCheck],
                    borderColor: rgbList[rgbCheck],
                    data: network.output.MetricDataResults[0].Values.reverse()
                });
                makeChart(labels1, datasets1, 'line', relayNetworkInMetricChart);
                makeChart(labels2, datasets2, 'line', relayNetworkOutMetricChart);
                const network1Div = document.createElement('div');
                const network2Div = document.createElement('div');
                network1Div.style.width = "30%";
                network2Div.style.width = "30%";
                network1Div.append(relayNetworkInMetricChart);
                network2Div.append(relayNetworkOutMetricChart);
                liveMetricData.append(network1Div);
                liveMetricData.append(network2Div);
                res(body);
            }
        }
        xhr.send();
    });
}

async function getLiveProxyMetric() {
    new Promise((res, rej) => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', `/metric/live/proxy`, true);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onreadystatechange = function () {
            let body = xhr.responseText;
            if (xhr.readyState === 4) {
                const data = JSON.parse(body);
                const {cpu, error4xx, error5xx} = data;
                const cpuCanvas = document.createElement('canvas');
                const error4xxCanvas = document.createElement('canvas');
                const error5xxCanvas = document.createElement('canvas');
                const cpuLabels = getChartLabel(cpu.MetricDataResults);
                const error4xxLabels = getChartLabel(error4xx.MetricDataResults);
                const error5xxLabels = getChartLabel(error5xx.MetricDataResults);
                const cpuDatasets = [{
                    label: 'proxy-cpu',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: cpu.MetricDataResults[0].Values.reverse()
                }];
                const error4xxDatasets = [{
                    label: 'proxy-4xx',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: error4xx.MetricDataResults[0].Values.reverse()
                }];
                const error5xxDatasets = [{
                    label: 'proxy-5xx',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: error5xx.MetricDataResults[0].Values.reverse()
                }];
                makeChart(cpuLabels, cpuDatasets, 'line', cpuCanvas);
                makeChart(error4xxLabels, error4xxDatasets, 'line', error4xxCanvas);
                makeChart(error5xxLabels, error5xxDatasets, 'line', error5xxCanvas);
                const cpuDiv = document.createElement('div');
                const error4xxDiv = document.createElement('div');
                const error5xxDiv = document.createElement('div');
                cpuDiv.style.width = "30%";
                error4xxDiv.style.width = "30%";
                error5xxDiv.style.width = "30%";
                cpuDiv.append(cpuCanvas);
                error4xxDiv.append(error4xxCanvas);
                error5xxDiv.append(error5xxCanvas);
                liveCommonMetricData.append(cpuDiv);
                liveCommonMetricData.append(error4xxDiv);
                liveCommonMetricData.append(error5xxDiv);
                res(body);
            }
        }
        xhr.send();
    });
}

async function setMetric() {
    const consumerIdInput = document.getElementById('consumerId');
    const consumerId = consumerIdInput.value;
    liveMetricData.innerText = "";
    liveMetricData.innerHTML = "";
    await getLiveEncoderMetric(consumerId);
    await getLiveRelayMetric(consumerId);
    await getLiveProxyMetric();
}

