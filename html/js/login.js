const query = getUrlParams();
const code = query.code;
const loginDiv = document.getElementById('loginDiv');
let user = '';

async function setHref() {
    const host = window.location.host;
    await new Promise((res, rej) => {
        const httpReq = new XMLHttpRequest();
        httpReq.open('GET', `/login?host=${host}`);
        httpReq.onreadystatechange = function () {
            let body = httpReq.responseText;
            if (httpReq.readyState === 4) {
                window.location.href = body;
            }
        };
        httpReq.send();
    });
}

async function getSlackUrl(code) {
    const host = window.location.host;
    return await new Promise((res, rej) => {
        const httpReq = new XMLHttpRequest();
        httpReq.open('GET', `/auth?host=${host}&code=${code}`);
        httpReq.onreadystatechange = function () {
            let body = httpReq.responseText;
            if (httpReq.readyState === 4) {
                res(body);
            }
        };
        httpReq.send();
    });
}

async function callSlack(code) {
    const requestUrl = await getSlackUrl(code);
    await new Promise((res, rej) => {
        const httpReq = new XMLHttpRequest();
        httpReq.open('POST', requestUrl);
        httpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        httpReq.onreadystatechange = function () {
            let body = httpReq.responseText;
            if (httpReq.readyState === 4) {
                const {authed_user} = JSON.parse(body);
                const {access_token} = authed_user;
                setCookies('token', access_token);
                getUserIdentity(access_token).then(()=>{});
            }
        };
        httpReq.send();
    });
}

async function getUserIdentity(accessToken) {
    console.log(accessToken);
    const formData = new FormData();
    formData.append('token', accessToken);
    await new Promise((res, rej) => {
        const httpReq = new XMLHttpRequest();
        httpReq.open('POST', `https://slack.com/api/users.identity`);
        // httpReq.setRequestHeader('Content-Type', 'application/json');
        httpReq.onreadystatechange = function () {
            let body = httpReq.responseText;
            if (httpReq.readyState === 4) {
                console.log(JSON.parse(body));
                setCookies('user', body);
                window.location.href = './';
            }
        };
        httpReq.send(formData);
    });
}

function setCookies(cookie, name) {
    let exDate = new Date();
    exDate.setDate(exDate.getDate() + 1);

    let cookieValue = escape(name) + ((1 == null) ? '' :'; expires='+ exDate.toISOString());
    document.cookie = cookie +'='+cookieValue +"; path=/";
}

function removeCookies(name) {
    document.cookie = name +'=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
}

function getCookie(cookie) {
    let x, y;
    let val = document.cookie.split(';');

    for (let i = 0; i < val.length; i++) {
        x = val[i].substr(0, val[i].indexOf('='));
        y = val[i].substr(val[i].indexOf('=') + 1);
        x = x.replace(/^\s+|\s+$/g, '');
        if (x === cookie) {
            return unescape(y);
        }
    }
}

function getUrlParams() {
    let params = {};
    window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(str, key, value) { params[key] = value; });
    return params;
}

const token = getCookie('token');
if (token) {
    const userCookie = getCookie('user');
    const userData = JSON.parse(userCookie);
    if (!userData.ok) {
        removeCookies('token');
        removeCookies('user');
        window.location.href = "./";
    }
    const userName = document.getElementById('userName');
    userName.innerText = userData.user.name;
    userName.addEventListener("click", async function () {
        alert(`user id = `+userData.user.id);
    });
    user = userData.user;
    console.log(userData);
} else {
    if (code) {
        callSlack(code).then(() => null);
    } else {
        setHref().then(() => null);
    }
}
