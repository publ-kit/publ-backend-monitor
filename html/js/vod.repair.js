const page = query?.page? query.page : 1;
const vodDataPage = document.getElementById('vodDataPage');
let totalPageCount = 0;
let defaultPages = query?.page? Number(query.page) + 9 : 10;
vodDataPage.innerText = page.toString();
async function call(method, url, data) {
    await new Promise((res, rej) => {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        xhr.setRequestHeader('x-monit-user-id', user.id);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onreadystatechange = function () {
            let body = xhr.responseText;
            if (xhr.readyState === 4) {
                getVODsCallback(body).then();
                res(xhr.responseText);
            }
        }
        xhr.send(data);
    });
}

async function callRepair(method, url) {
    await new Promise((res, rej) => {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        xhr.setRequestHeader('x-monit-user-id', user.id);
        xhr.onreadystatechange = function () {
            let body = xhr.responseText;
            if (xhr.readyState === 4) {
                console.log(body);
                const data = JSON.parse(body);
                if (data.message) {
                    alert(data.message);
                }
                location.href = `./?page=${vodDataPage.innerText}`;
                res(xhr.responseText);
            }
        }
        xhr.send();
    });
}

async function callAndGetRes(method, url) {
    return await new Promise((res, rej) => {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        xhr.setRequestHeader('x-monit-user-id', user.id);
        xhr.onreadystatechange = function () {
            let body = xhr.responseText;
            if (xhr.readyState === 4) {
                console.log(body);
                res(body);
                return body;
            }
        }
        xhr.send();
    });
}

searchVODById = async function() {
    const vodData = document.getElementById('vodData');
    vodData.innerText = "";
    const vidInput = document.getElementById('vidInput');
    await call('GET', `/vods?page=1&size=10&id=${vidInput.value}`);
}

function getButtons(num) {
    const buttons = [];
    for (let i=num-9;i<=num;i++){
        const button = document.createElement('p');
        button.addEventListener('click', async function () {
            const vodData = document.getElementById('vodData');
            vodData.innerText = "";
            vodDataPage.innerText = i.toString();
            await call('GET', `/vods?page=${i}&size=10`)
            history.pushState(null, null, `?page=${i}`);
        });
        button.innerText = i;
        button.className = 'pagination';
        button.id = 'btn1';
        buttons.push(button);
    }
    return buttons;
}

async function getVODCount (){
    const res = await callAndGetRes('GET', '/vods/count');
    totalPageCount = Number(res);
    const buttons = getButtons(defaultPages);
    const btnDiv = document.getElementById('btnDiv');
    btnDiv.innerText = "";
    buttons.map((button) => {
        btnDiv.appendChild(button)
    });
}

getVODsCallback = async function (data) {
    const vodData = document.getElementById('vodData');
    const table = document.createElement('table');
    table.id = 'repairTable';
    const thead = document.createElement('thead');
    const tbody = document.createElement('tbody');
    const headTr = document.createElement('tr');
    const idTh = document.createElement('th');
    const cidTh = document.createElement('th');
    const pidTh = document.createElement('th');
    const statusTh = document.createElement('th');
    const playTimeTh = document.createElement('th');
    const vodStatusTh = document.createElement('th');
    const vodMultiStatusTh = document.createElement('th');
    const contentsQualityTh = document.createElement('th');
    const contentsStatusTh = document.createElement('th');
    const contentsPlaylistTh = document.createElement('th');
    const subtitleTh = document.createElement('th');
    const subtitleStatusTh = document.createElement('th');
    const contentsS3Th = document.createElement('th');
    const createdAtTh = document.createElement('th');
    const updatedAtTh = document.createElement('th');
    const repairTh = document.createElement('th');
    const consumerIdsTh = document.createElement('th');
    idTh.innerText = 'id';
    cidTh.innerText = 'cid';
    pidTh.innerText = 'pid';
    statusTh.innerText = 'status';
    playTimeTh.innerText = 'playTime';
    vodStatusTh.innerText = 'vodStatus';
    vodMultiStatusTh.innerText = 'multiPlaylistStatus';
    contentsQualityTh.innerText = 'quality';
    contentsStatusTh.innerText = 'status';
    contentsPlaylistTh.innerText = 'playlist';
    subtitleTh.innerText = 'subtitle';
    subtitleStatusTh.innerText = 'subtitle status';
    contentsS3Th.innerText = 's3';
    createdAtTh.innerText = 'createdAt';
    updatedAtTh.innerText = 'updatedAt';
    repairTh.innerText = 'repair';
    consumerIdsTh.innerText = 'consumerIds';
    headTr.appendChild(idTh);
    headTr.appendChild(cidTh);
    headTr.appendChild(pidTh);
    headTr.appendChild(statusTh);
    headTr.appendChild(playTimeTh);
    headTr.appendChild(vodStatusTh);
    headTr.appendChild(vodMultiStatusTh);
    headTr.appendChild(contentsQualityTh);
    headTr.appendChild(contentsStatusTh);
    headTr.appendChild(contentsPlaylistTh);
    headTr.appendChild(contentsS3Th);
    headTr.appendChild(subtitleTh);
    headTr.appendChild(subtitleStatusTh);
    headTr.appendChild(repairTh);
    headTr.appendChild(consumerIdsTh);
    headTr.appendChild(createdAtTh);
    headTr.appendChild(updatedAtTh);
    thead.appendChild(headTr);
    table.appendChild(thead);
    table.appendChild(tbody);
    vodData.appendChild(table);
    const vods = JSON.parse(data);
    for (const vod of vods) {
        const {id, cid, pid, status, playTime, contentsStatus, createdAt, updatedAt} = vod;
        const bodyTr = document.createElement('tr');
        const idTd = document.createElement('td');
        idTd.innerText = id;
        const cidTd = document.createElement('td');
        cidTd.innerText = cid;
        const consumerIdsTd = document.createElement('td');
        const getConsumerIdsUrl = `/vods/consumer-ids/${cid}`;
        const consumerIdsData = await callAndGetRes('GET', getConsumerIdsUrl);
        const consumerIds = JSON.parse(consumerIdsData);
        for (const consumerId of consumerIds) {
            consumerIdsTd.innerHTML += `<p><input onclick="playPreview('${consumerId}', '${id}')" value="${consumerId}"> </p>`;
        }
        const pidTd = document.createElement('td');
        pidTd.innerText = pid;
        const statusTd = document.createElement('td');
        statusTd.innerText = status
        const playTimeTd = document.createElement('td');
        playTimeTd.innerText = playTime
        const vodStatusTd = document.createElement('td');
        const vodMultiStatusTd = document.createElement('td');
        const contentsQualityTd = document.createElement('td');
        const contentsStatusTd = document.createElement('td');
        const contentsPlaylistTd = document.createElement('td');
        const contentsS3Td = document.createElement('td');
        const subtitleTd = document.createElement('td');
        const subtitleStatusTd = document.createElement('td');
        const repairTd = document.createElement('td');
        const repairBtn = document.createElement('input');
        repairBtn.type = 'button';
        repairBtn.value = 'repair';
        vodStatusTd.innerText = (status === 1);
        const vodQualityStatus = contentsStatus.vodQuality;
        const vodMultiStatus = contentsStatus.multiPlaylist;
        vodMultiStatusTd.innerText = vodMultiStatus;
        vodStatusTd.style.backgroundColor = (status === 1)? '#00ff00' : '#ff0000';
        vodMultiStatusTd.style.backgroundColor = (vodMultiStatus)? '#00ff00' : '#ff0000';
        let canRepair = false;
        for (const qualityData of vodQualityStatus) {
            const {quality, status, playlist, s3} = qualityData;
            contentsQualityTd.innerHTML += `<p>quality : ${quality}</p>`;
            const contentsStatusColor = status? '#00ff00' : '#ff0000';
            contentsStatusTd.innerHTML += `<p style="background-color: ${contentsStatusColor}"> status : ${status} </p>`;
            const contentsPlaylistColor = playlist? '#00ff00' : '#ff0000';
            contentsPlaylistTd.innerHTML += `<p style="background-color: ${contentsPlaylistColor}">playlist: ${playlist}</p>`;
            const contentsS3Color = s3? '#00ff00' : '#ff0000';
            contentsS3Td.innerHTML += `<p style="background-color: ${contentsS3Color}">s3 : ${s3}</p>`;
            if (s3) canRepair = true;
        }
        const subtitleData = contentsStatus.subtitle;
        for (const subtitle of subtitleData) {
            const {language, status, isDefault} = subtitle;
            subtitleTd.innerHTML += `<p>${language} : | default : ${isDefault}</p>`;
            const subtitleStatusTdColor = (status===1)? '#00ff00' : '#ff0000';
            subtitleStatusTd.innerHTML += `<p style="background-color: ${subtitleStatusTdColor}"> ${status} </p>` ;
        }
        repairBtn.addEventListener('click', function () {
                if (canRepair) {
                    callRepair('PATCH', `/vods/${id}/repair`);
                } else {
                    alert('This VOD is invalid');
                }
            }
        );
        repairTd.appendChild(repairBtn);
        const createdAtTd = document.createElement('td');
        createdAtTd.innerText = createdAt
        const updatedAtTd = document.createElement('td');
        updatedAtTd.innerText = updatedAt;
        bodyTr.appendChild(idTd);
        bodyTr.appendChild(cidTd);
        bodyTr.appendChild(pidTd);
        bodyTr.appendChild(statusTd);
        bodyTr.appendChild(playTimeTd);
        bodyTr.appendChild(vodStatusTd);
        bodyTr.appendChild(vodMultiStatusTd);
        bodyTr.appendChild(contentsQualityTd);
        bodyTr.appendChild(contentsStatusTd);
        bodyTr.appendChild(contentsPlaylistTd);
        bodyTr.appendChild(contentsS3Td);
        bodyTr.appendChild(subtitleTd);
        bodyTr.appendChild(subtitleStatusTd);
        bodyTr.appendChild(repairTd);
        bodyTr.appendChild(consumerIdsTd);
        bodyTr.appendChild(createdAtTd);
        bodyTr.appendChild(updatedAtTd);
        tbody.appendChild(bodyTr);
    }
}
async function vodDataPagingUp() {
    defaultPages = (defaultPages +10 > totalPageCount)? totalPageCount : defaultPages+10;
    const buttons = getButtons(defaultPages);
    const btnDiv = document.getElementById('btnDiv');
    btnDiv.innerText = "";
    buttons.map((button) => {
        btnDiv.appendChild(button)
    });
}

async function vodDataPagingDown() {
    defaultPages = (defaultPages < 20)? 10 : defaultPages-10;
    const buttons = getButtons(defaultPages);
    const btnDiv = document.getElementById('btnDiv');
    btnDiv.innerText = "";
    buttons.map((button) => {
        btnDiv.appendChild(button)
    });
}

function getUrlParams() {
    let params = {};
    window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(str, key, value) { params[key] = value; });
    return params;
}

async function playPreview(consumerId, vid) {
    const playToken = await callAndGetRes('GET', `/vods/${vid}/token?consumerId=${consumerId}`);
    const host = window.location.host;
    let playUrl = `https://vod-proxy.publishingkit.net/${vid}/index.m3u8?token=${playToken}`;
    if (host.indexOf('dev') > -1) {
        playUrl = `https://vod-proxy.dev.publishingkit.net/${vid}/index.m3u8?token=${playToken}&preview=1`;
    } else if (host.indexOf('local') > -1){
        playUrl = `https://vod-proxy.local.publishingkit.net/${vid}/index.m3u8?token=${playToken}&preview=1`;
    }
    window.open(
        playUrl,
        '_blank'
    );
}
getVODCount().then();
call('GET', `/vods?page=${page}&size=10`).then();
