

const targetList = ["VOD-PROXY", "VOD-USER", "VOD-SELLER", "LIVE-PROXY", "LIVE-USER", "LIVE-SELLER"];
const metricList = ["TargetResponseTime", "RequestCount", "HTTPCode_Target_5XX_Count", "HTTPCode_Target_4XX_Count"]
const targetSelector = document.getElementById('targetSelector');
const metricSelector = document.getElementById('metricSelector');
const monitCharDiv = document.getElementById('monitCharDiv');

function getLogStream() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', '/logs/stream', true);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.onreadystatechange = function () {
        let body = xhr.responseText;
        if (xhr.readyState === 4) {
            console.log(body);
        }
        // location.reload();
    }
    xhr.send(data);
}


function setTargetSelector() {
    for (let target of targetList) {
        const option = document.createElement("option");
        option.value = target;
        option.innerText = target;
        targetSelector.append(option);
    }
}

function setMetricSelector() {
    for (let metric of metricList) {
        const option = document.createElement("option");
        option.value = metric;
        option.innerText = metric;
        metricSelector.append(option);
    }
}

function getMetricData(){
    const target = targetSelector.value;
    const metric = metricSelector.value;
    let startTime = document.getElementById('startTime').value;
    let endTime = document.getElementById('endTime').value;

    if (startTime) {
        startTime = new Date(startTime).toISOString();
    } else {
        let now = new Date();
        let target = new Date();
        target.setHours(now.getHours() - 3);
        startTime = target.toISOString();
        document.getElementById('startTime').value = startTime;
    }
    if (endTime) {
        endTime = new Date(endTime).toISOString();
    } else {
        endTime = new Date().toISOString();
        document.getElementById('endTime').value = endTime;
    }
    console.log(target);
    console.log(metric);
    console.log(startTime);
    console.log(endTime);
    let xhr = new XMLHttpRequest();
    xhr.open('GET', '/logs/stream?'+`target=${target}&metricName=${metric}&startTime=${startTime}&endTime=${endTime}`, true);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.onreadystatechange = function () {
        let body = xhr.responseText;
        if (xhr.readyState === 4) {
            const data = JSON.parse(body);
            console.log(data.MetricDataResults[0]);
            const MetricDataResult = data.MetricDataResults[0]
            const labels = MetricDataResult.Timestamps.reverse();
            const chartData = {
                labels,
                datasets: [{
                    label: MetricDataResult.Label,
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: MetricDataResult.Values.reverse()
                }]
            };

            const config = {
                type: 'line',
                data: chartData,
                options: {}
            };

            monitCharDiv.innerText = "";
            monitCharDiv.innerHTML = "";
            const monitChart = document.createElement('canvas');
            new Chart(monitChart, config);
            monitCharDiv.append(monitChart);
        }
    }
    xhr.send();
}

function clearCanvas() {
    // context
    let ctx = monitChart.getContext('2d');
    ctx.clearRect(0, 0, monitChart.width, monitChart.height);
}

setMetricSelector();
setTargetSelector();
