user  root;
worker_processes  2;

error_log  logs/error.log  error;

events {
    worker_connections  10000;
}

env JWT_KEY;
env ENV_NAME;
env REDIS_HOST;
env MQ_USER;
env MQ_PASS;
env MQ_HOST;
env MQ_PORT;
env MQ_NAME;
env PG_HOST;
env PG_PORT;
env PG_USER;
env PG_PASS;
env PG_DB;
env SIG_REDIS;
env LIVE_REDIS;
env REDIS_PORT;
env CF_DOMAIN;
env AES_KEY;
env AES_IV;
env KEY_URI;

http {
    include       mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
    log_format	access '{"time": "$time_iso8601", "status": $status, "requestTime": $request_time, "contentLength": $body_bytes_sent, '
	              '"method": "$request_method", "requestPath": "$reqPath", "ua":"$http_user_agent"}';

    access_log  logs/access.log  access;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;

    #gzip  on;
    lua_code_cache off;
    resolver 127.0.0.11;

    server {
        listen       8801;
    	set $reqPath '';

        location / {
            root   /data/html;
            index  index.html index.htm;
        }

		location /lives {
			content_by_lua_file lua/monitorGetLive.lua;
		}

        location /live/end-time-update {
            proxy_pass "http://publ-backend-api:3300/api/v1$request_uri";
        }

        location /live/start-time-update {
            proxy_pass "http://publ-backend-api:3300/api/v1$request_uri";
        }

        location /backup {
            content_by_lua_file lua/monitorBackupLive.lua;
        }

		location /redis-status {
			content_by_lua_file lua/monitorGetRedis.lua;
		}

		location /encoder-status {
			content_by_lua_file lua/monitorGetEncoder.lua;
		}

# 		location /call-sync {
# 			content_by_lua_file lua/monitorMain.lua;
# 		}

		location ~ ^/(batch|logs|sync|destroy|login|auth|queues|jobQueues|jobqueues)$ {
		    proxy_pass "http://publ-backend-api:3300/api/v1$request_uri";
		}

        location ~ ^/batch/(.*)$ {
            proxy_pass "http://publ-backend-api:3300/api/v1$request_uri";
        }

		location ~ ^/logs/stream$ {
		    proxy_pass "http://publ-backend-api:3300/api/v1$request_uri";
		}

        location ~ ^/logs/encoding$ {
            proxy_pass "http://publ-backend-api:3300/api/v1$request_uri";
        }
        location ~ ^/metric/live/(encoder|relay|proxy)$ {
            proxy_pass "http://publ-backend-api:3300/api/v1$request_uri";
        }

        location ~ ^/vods(.*)$ {
            proxy_pass "http://publ-backend-api:3300/api/v1$request_uri";
        }

        location ~ ^/live-histories(.*)$ {
            proxy_pass "http://publ-backend-api:3300/api/v1$request_uri";
        }

		location ~ ^/live/key/(.*)$ {
		    set $liveId $1;
            if ($request_method = 'OPTIONS') {
                add_header 'Access-Control-Allow-Origin' '*';
                add_header 'Access-Control-Allow-Credentials' 'true';
                add_header 'Access-Control-Allow-Headers' 'Authorization,Accept,Origin,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range,x-publ-channel-id';
                add_header 'Access-Control-Allow-Methods' 'GET,POST,OPTIONS,PUT,DELETE,PATCH';
                add_header 'Access-Control-Max-Age' 1728000;
                add_header 'Content-Type' 'text/plain charset=UTF-8';
                add_header 'Content-Length' 0;
                return 204;
            }
            content_by_lua_file lua/monitorPlayLiveKey.lua;
		}

		location ~ ^/live/(.*)/(.*)/playlist.m3u8$ {
            if ($request_method = 'OPTIONS') {
                add_header 'Access-Control-Allow-Origin' '*';
                add_header 'Access-Control-Allow-Credentials' 'true';
                add_header 'Access-Control-Allow-Headers' 'Authorization,Accept,Origin,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range,x-publ-channel-id';
                add_header 'Access-Control-Allow-Methods' 'GET,POST,OPTIONS,PUT,DELETE,PATCH';
                add_header 'Access-Control-Max-Age' 1728000;
                add_header 'Content-Type' 'text/plain charset=UTF-8';
                add_header 'Content-Length' 0;
                return 204;
            }
            add_header 'Access-Control-Allow-Origin' '*' always;
            add_header 'Access-Control-Allow-Credentials' 'true' always;
            add_header 'Access-Control-Allow-Headers' 'Authorization,Accept,Origin,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range' always;
            add_header 'Access-Control-Allow-Methods' 'GET,OPTIONS,HEAD' always;
            add_header 'Cache-Control' 'no-cache';
            set $streamKey $1;
            set $liveId $2;
		    content_by_lua_file lua/monitorPlayLivePlaylist.lua;
		}

		location ~ ^/live/(.*)/(.*)/(.*).ts$ {
            if ($request_method = 'OPTIONS') {
                add_header 'Access-Control-Allow-Origin' '*';
                add_header 'Access-Control-Allow-Credentials' 'true';
                add_header 'Access-Control-Allow-Headers' 'Authorization,Accept,Origin,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range,x-publ-channel-id';
                add_header 'Access-Control-Allow-Methods' 'GET,POST,OPTIONS,PUT,DELETE,PATCH';
                add_header 'Access-Control-Max-Age' 1728000;
                add_header 'Content-Type' 'text/plain charset=UTF-8';
                add_header 'Content-Length' 0;
                return 204;
            }
            add_header 'Access-Control-Allow-Origin' '*' always;
            add_header 'Access-Control-Allow-Credentials' 'true' always;
            add_header 'Access-Control-Allow-Headers' 'Authorization,Accept,Origin,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range' always;
            add_header 'Access-Control-Allow-Methods' 'GET,OPTIONS,HEAD' always;
            add_header 'Cache-Control' 'no-cache';
            types {
                applicaiont/vnd.apple.mpegurl m3u8;
                video/mp2t ts;
            }
            proxy_hide_header access-control-allow-headers:;
            proxy_hide_header access-control-allow-methods;
            proxy_hide_header access-control-allow-origin;
            proxy_hide_header access-control-expose-headers;
            add_header Cache-Control no-cache;
            set $streamKey $1;
            set $liveId $2;
            set $tsName $3;
            rewrite_by_lua_file lua/monitorPlayLiveTs.lua;
		}
    }
}
